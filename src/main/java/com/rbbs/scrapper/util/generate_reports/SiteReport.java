package com.rbbs.scrapper.util.generate_reports;

import com.rbbs.scrapper.parsers.site.model.Site;
import com.rbbs.scrapper.parsers.site.model.Tender;
import com.rbbs.scrapper.shared.LoggerHelper;
import com.rbbs.scrapper.util.file_storages.WindowsFileStorage;
import com.rbbs.scrapper.util.file_storages.interfaces.FileStorage;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class SiteReport {
    private static final Logger fileLogger = Logger.getLogger("APP2");

    public static void createReport(@NotNull Site site, String pathToDirectory) {
        FileStorage fileStorage = new WindowsFileStorage();

        File reportFile = fileStorage.createNewFile(pathToDirectory, "Отчет по всем закупкам.txt");
        try {
            fileStorage.writeInFile(reportFile, "Отчет по выполнению программы RBBS.Scrapper. Дата и время запуска: " + LocalDateTime.now().toString() + "\r\n");
            fileStorage.writeInFile(reportFile, "***\r\nURL полученных страниц согласно запросу:\r\n");
            for (int i = 0; i < site.getAllListedTendersLinks().size(); i++) {
                fileStorage.writeInFile(reportFile, i + 1 + "\r\n" + site.getAllListedTendersLinks().get(i));
            }

            List<Tender> success = new ArrayList<>();
            List<Tender> noAttachments = new ArrayList<>();
            List<Tender> noReportsTenders = new ArrayList<>();
            List<Tender> tenderWithReports = new ArrayList<>();
            for (Tender tender : site.getTenders()) {
                if (tender.getLinksToAttachments().size() > 0) {
                    success.add(tender);
                } else {
                    noAttachments.add(tender);
                }
                File[] files = tender.getDirectory().listFiles();
                boolean checkReports = false;
                if (files != null) {
                    for (File file : files) {
                        if (file.getName().contains("Отчет_")) {
                            checkReports = true;
                        }
                    }
                }
                if (!checkReports) {
                    noReportsTenders.add(tender);
                } else {
                    tenderWithReports.add(tender);
                }
            }

            fileStorage.writeInFile(reportFile, "\r\n***\r\nURL закупок с отчетами:\r\n");
            for (int i = 0; i < tenderWithReports.size(); i++) {
                fileStorage.writeInFile(reportFile, i + 1 + "\r\nLink: " + tenderWithReports.get(i).getLinkToTender());
                fileStorage.writeInFile(reportFile, "Path to directory: " + tenderWithReports.get(i).getDirectory().getAbsolutePath() + "\r\n");
            }

            fileStorage.writeInFile(reportFile, "***\r\nURL закупок без отчетов:\r\n");
            for (int i = 0; i < noReportsTenders.size(); i++) {
                fileStorage.writeInFile(reportFile, i + 1 + "\r\nLink: " + noReportsTenders.get(i).getLinkToTender());
                fileStorage.writeInFile(reportFile, "Path to directory: " + noReportsTenders.get(i).getDirectory().getAbsolutePath() + "\r\n");
            }

            fileStorage.writeInFile(reportFile, "***\r\nURL закупок с вложениями:\r\n");
            for (int i = 0; i < success.size(); i++) {
                fileStorage.writeInFile(reportFile, i + 1 + "\r\nLink:" + success.get(i).getLinkToTender());
                fileStorage.writeInFile(reportFile, "Path to directory: " + success.get(i).getDirectory().getAbsolutePath() + "\r\n");
            }

            fileStorage.writeInFile(reportFile, "***\r\nURL закупок без вложений:\r\n");
            for (int i = 0; i < noAttachments.size(); i++) {
                fileStorage.writeInFile(reportFile, i + 1 + "\r\nLink: " + noAttachments.get(i).getLinkToTender());
                fileStorage.writeInFile(reportFile, "Path to directory: " + noAttachments.get(i).getDirectory().getAbsolutePath() + "\r\n");
            }
        } catch (IOException e) {
            LoggerHelper.error("Ошибка создания отчета RBBS.Scrapper", e, fileLogger);
        }
    }
}
