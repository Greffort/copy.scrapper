package com.rbbs.scrapper.util.generate_reports;

import com.rbbs.scrapper.parsers.site.model.Tender;
import com.rbbs.scrapper.shared.LoggerHelper;
import com.rbbs.scrapper.util.file_storages.WindowsFileStorage;
import com.rbbs.scrapper.util.file_storages.interfaces.FileStorage;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

public class TenderReport {
    private static final Logger fileLogger = Logger.getLogger("APP2");

    public static void createReport(@NotNull Tender tender) {
        FileStorage fileStorage = new WindowsFileStorage();
        File reportFile = fileStorage.createNewFile(tender.getDirectory().getAbsolutePath(), "Отчет по закупке.txt");

        try {
            fileStorage.writeInFile(reportFile, "Файл: " + tender.getDirectory().getAbsolutePath());
            fileStorage.writeInFile(reportFile, "\r\nЭтап закупки: " + tender.getTenderStep());
            fileStorage.writeInFile(reportFile, "\r\nНомер закупки: " + tender.getTenderNumber());
            fileStorage.writeInFile(reportFile, "\r\nОбъект закупки: " + tender.getDescription());
            fileStorage.writeInFile(reportFile, "\r\nСсылка на закупку: " + tender.getLinkToTender());
            fileStorage.writeInFile(reportFile, "\r\nНачальная цена: " + tender.getStartPrice());
            fileStorage.writeInFile(reportFile, "\r\nЦена контракта: " + tender.getLastPrice());
            fileStorage.writeInFile(reportFile, "\r\nРазмещающая организация: " + tender.getHostingOrganization());
            fileStorage.writeInFile(reportFile, "\r\nПоставщик: " + tender.getWinner());
            fileStorage.writeInFile(reportFile, "\r\nИНН поставщика: " + tender.getWinnerINN());
            fileStorage.writeInFile(reportFile, "\r\nСсылки на закупки: ");
            for (Map.Entry<String, String> entry : tender.getLinksToAttachments().entrySet()) {
                fileStorage.writeInFile(reportFile, "\r\nИмя файла: " + entry.getKey() + "\r\nСсылка: " + entry.getValue());
            }
        } catch (IOException e) {
            LoggerHelper.error("Ошибка создания отчета RBBS.Scrapper", e, fileLogger);
        }
    }
}
