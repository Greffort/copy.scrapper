package com.rbbs.scrapper.util;

import com.rbbs.scrapper.shared.LoggerHelper;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MomKiller {

    private static final Logger fileLogger = Logger.getLogger("APP2");

    public static boolean isProcessRunning(String serviceName) {
        Process p;
        try {
            p = Runtime.getRuntime().exec("TASKLIST");
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.contains(serviceName)) {
                    return true;
                }
            }
        } catch (Exception e) {
            LoggerHelper.info("Process webdriver.exe not found.1", fileLogger);
        }
        return false;
    }

    public static void killProcess(String serviceName) {
        try {
            Runtime.getRuntime().exec("taskkill /F /IM " + serviceName + " /T");
        } catch (Exception e) {
            LoggerHelper.info("Process webdriver.exe not found.2", fileLogger);
        }
    }
}
