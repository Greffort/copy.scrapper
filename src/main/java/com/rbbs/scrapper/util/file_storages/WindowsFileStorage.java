package com.rbbs.scrapper.util.file_storages;

import com.github.junrar.exception.RarException;
import com.github.junrar.extract.ExtractArchive;
import com.rbbs.scrapper.shared.LoggerHelper;
import com.rbbs.scrapper.util.file_storages.interfaces.FileStorage;
import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.rauschig.jarchivelib.Archiver;
import org.rauschig.jarchivelib.ArchiverFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class WindowsFileStorage implements FileStorage {
    private static final Logger logConsole = Logger.getLogger("APP1");
    private static final Logger logFile = Logger.getLogger("APP2");

    public String getFileExtension(@NotNull File file) {
        return FilenameUtils.getExtension(file.getName());
    }

    public boolean checkFileExtension(File file, @NotNull List<String> extensions) {
        for (String ext : extensions) {
            if (file == null) {
                return false;
            } else if (file.getName().endsWith(ext)) {
                return true;
            }
        }
        return false;
    }

    public File createFile(String pathToFile, String fileName) {
        LoggerHelper.info("createFile", logFile);
        File file = new File(pathToFile, fileName);
        try {
            file.createNewFile();
        } catch (IOException e) {
            LoggerHelper.error(e, logFile);
        }
        return file;
    }

    @Override
    public File createNewFile(String pathToFile, String fileName) {
        LoggerHelper.info("createNewFile", logFile);
        File file = new File(pathToFile, fileName);
        if (file.exists()) {
            removeFile(file);
        }
        try {
            file.createNewFile();
        } catch (IOException e) {
            LoggerHelper.error(e, logFile);
        }
        return file;
    }

    @Override
    public void removeFile(String pathToFile, String fileName) {
        LoggerHelper.info("removeFile (String, String)", logFile);
        File file = new File(pathToFile, fileName);
        file.delete();
    }

    @Override
    public void removeFile(@NotNull File file) {
        LoggerHelper.info("removeFile (File)", logFile);
        file.delete();
    }

    @Override
    public File createDirectory(String pathToDirectory, String directoryName) {
        LoggerHelper.info("createDirectory (String, String)", logFile);
        File directory = new File(pathToDirectory, directoryName);
        directory.mkdir();
        return directory;
    }

    public File createDirectory(String fullPathToDirectory) {
        LoggerHelper.info("createDirectory (String)", logFile);
        File directory = new File(fullPathToDirectory);
        directory.mkdir();
        return directory;
    }

    @Override
    public void removeDirectory(String pathToDirectory, String directoryName) {
        LoggerHelper.info("removeDirectory (String, String)", logFile);
        File directory = new File(pathToDirectory, directoryName);
        directory.delete();
    }

    @Override
    public void removeDirectory(@NotNull File directory) {
        LoggerHelper.info("removeDirectory (File)", logFile);
        directory.delete();
    }

    @Override
    public void copyDirectory(String srcPath, String destPath, String directoryName) {

    }

    @Override
    public void moveFile(String srcPath, String destPath, String fileName) {

    }

    @Override
    public void writeInFile(File file, String line) throws IOException {
        FileWriter writer = new FileWriter(file, true);
        writer.write(line);
        writer.append('\n');
        writer.flush();
    }

    //проверить этот метод, он пытается переместить файлы которые уже существуют в этой директории
    @Override
    public void moveFiles(@NotNull File directory, File destDirectory) throws IOException {
        File[] files = directory.listFiles();
        if (files != null) {
            for (File entry : files) {
                if (entry.isDirectory()) {
                    moveFiles(entry, destDirectory);
                } else {
                    if (!new File(destDirectory, entry.getName()).exists()) {
                        FileUtils.copyToDirectory(entry, destDirectory);
                    }
                }
            }
        }
    }

    @Override
    public List<String> getFilesNameInDirectory(@NotNull File directory) {
        List<String> result = new ArrayList<>();

        for (File file : Objects.requireNonNull(directory.listFiles())) {
            if (!file.getName().contains("~$")) {
                result.add(file.getName());
            }
        }
        return result;
    }

    @Override
    public List<File> getFilesInDirectory(@NotNull File directory) {
        List<File> result = new ArrayList<>();
        File[] files = directory.listFiles();
        if (files != null) {
            for (File entry : files) {
                if (!entry.getName().contains("~$")) {
                    result.add(entry);
                }
            }
        }
        return result;
    }

    @Override
    public boolean unarchive(@NotNull File file, File destDirectory) {
        LoggerHelper.info("unarchive", logFile);
        if (file.getName().endsWith(".zip")) {
            try {
                ZipFile zipFile = new ZipFile(file.getAbsoluteFile());
                zipFile.extractAll(destDirectory.getAbsolutePath());
                file.delete();
                return true;
            } catch (ZipException e) {
                LoggerHelper.error("Error unarchive zip file: ", e, logFile);
                LoggerHelper.error("Error unarchive zip file: " + file.getAbsolutePath(), logConsole);
                return false;
            }
        }
        if (file.getName().endsWith(".rar")) {
            try {
                ExtractArchive extractArchive = new ExtractArchive();
                extractArchive.extractArchive(file, destDirectory);
                file.delete();
                return true;
            } catch (IOException | RarException e) {
                LoggerHelper.error("Error unarchive rar file: ", e, logFile);
                LoggerHelper.error("Error unarchive rar file: " + file.getAbsolutePath(), logConsole);
                return false;
            }
        }
        if (file.getName().endsWith(".7z")) {
            try {
                Archiver archiver = ArchiverFactory.createArchiver(file);
                archiver.extract(file, destDirectory);
                file.delete();
                return true;
            } catch (Exception e) {
                LoggerHelper.error("Error unarchive 7z file: ", e, logFile);
                LoggerHelper.error("Error unarchive 7z file: " + file.getAbsolutePath(), logConsole);
                return false;
            }
        }
        return false;
    }

    @Override
    public boolean unarchiveEncoding(@NotNull File file, File destDirectory) {
        LoggerHelper.info("unarchiveEncoding", logFile);
        if (file.getName().endsWith(".zip")) {

        }
        if (file.getName().endsWith(".rar")) {
            try {
                ExtractArchive extractArchive = new ExtractArchive();
                extractArchive.extractArchive(file, destDirectory);
                removeFile(file);
                return true;
            } catch (IOException | RarException e) {
                LoggerHelper.error("Error unarchive rar file: ", e, logFile);
                LoggerHelper.error("Error unarchive rar file: " + file.getAbsolutePath(), logConsole);
                return false;
            }
        }
        return false;
    }

    @Override
    public boolean unarchiveAll(@NotNull File directory, File destDirectory) {
        LoggerHelper.info("unarchiveAll", logFile);
        File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                return unarchive(file, destDirectory);
            }
            return false;
        } else {
            return false;
        }
    }

    // рекурсия
    public void unarchiveAllRecursive(@NotNull File directory, File destDirectory) {
        LoggerHelper.info("unarchiveAllRecursive", logFile);
        File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    unarchiveAllRecursive(file, destDirectory);
                }
                if (file.getName().endsWith(".rar") || file.getName().endsWith(".zip") || file.getName().endsWith(".7z")) {
                    if (!unarchive(file, destDirectory)) {
                        return;
                    }
                }
            }
            for (File file : files) {
                if (file.getName().endsWith(".rar") || file.getName().endsWith(".zip") || file.getName().endsWith(".7z")) {
                    unarchiveAllRecursive(directory, destDirectory);
                }
            }
        }
    }

    // хороший метод, который разархивирует правильно, как надо, но юзает стандартную библиотеку.
//    static public void extractFolder(String zipFile) throws ZipException, IOException {
//        System.out.println(zipFile);
//        int BUFFER = 2048;
//        File file = new File(zipFile);
//
//        ZipFile zip = new ZipFile(file);
//        String newPath = zipFile.substring(0, zipFile.length() - 4);
//
//        new File(newPath).mkdir();
//        Enumeration zipFileEntries = zip.entries();
//
//        // Process each entry
//        while (zipFileEntries.hasMoreElements()) {
//            // grab a zip file entry
//            ZipEntry entry = (ZipEntry) zipFileEntries.nextElement();
//            String currentEntry = entry.getName();
//            File destFile = new File(newPath, currentEntry);
//            //destFile = new File(newPath, destFile.getName());
//            File destinationParent = destFile.getParentFile();
//
//            // create the parent directory structure if needed
//            destinationParent.mkdirs();
//
//            if (!entry.isDirectory()) {
//                BufferedInputStream is = new BufferedInputStream(zip
//                        .getInputStream(entry));
//                int currentByte;
//                // establish buffer for writing file
//                byte data[] = new byte[BUFFER];
//
//                // write the current file to disk
//                FileOutputStream fos = new FileOutputStream(destFile);
//                BufferedOutputStream dest = new BufferedOutputStream(fos,
//                        BUFFER);
//
//                // read and write until last byte is encountered
//                while ((currentByte = is.read(data, 0, BUFFER)) != -1) {
//                    dest.write(data, 0, currentByte);
//                }
//                dest.flush();
//                dest.close();
//                is.close();
//            }
//
//            if (currentEntry.endsWith(".zip")) {
//                // found a zip file, try to open
//                extractFolder(destFile.getAbsolutePath());
//            }
//        }
//    }
//      Create temp dir
//    File tmpDir = File.createTempFile("bip.", ".unrar");
//
//                if (!(tmpDir.delete())) {
//        throw new IOException("Could not delete temp file: " + tmpDir.getAbsolutePath());
//    }
//                if (!(tmpDir.mkdir())) {
//        throw new IOException("Could not create temp directory: " + tmpDir.getAbsolutePath());
//    }
}
