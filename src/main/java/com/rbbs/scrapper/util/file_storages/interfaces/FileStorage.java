package com.rbbs.scrapper.util.file_storages.interfaces;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface FileStorage {
    String getFileExtension(File file);

    File createDirectory(String pathToDirectory, String directoryName);

    File createDirectory(String pathToDirectory);

    void removeDirectory(String pathToDirectory, String directoryName);

    void removeDirectory(File directory);

    void copyDirectory(String srcPath, String destPath, String directoryName);

    File createFile(String pathToFile, String fileName);

    File createNewFile(String pathToFile, String fileName);

    void removeFile(String pathToFile, String fileName);

    void removeFile(File file);

    void moveFile(String srcPath, String destPath, String fileName);

    void writeInFile(File file, String line) throws IOException;

    void moveFiles(File directory, File destDirectory) throws IOException;

    boolean checkFileExtension(File fileName, List<String> extension);

    List<String> getFilesNameInDirectory(File directory);

    List<File> getFilesInDirectory(File directory);

    boolean unarchive(File file, File destDirectory);

    boolean unarchiveEncoding(File file, File destDirectory);

    boolean unarchiveAll(File directory, File destDirectory);

    void unarchiveAllRecursive(@NotNull File directory, File destDirectory);
}
