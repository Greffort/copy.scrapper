package com.rbbs.scrapper.controllers;

import com.rbbs.scrapper.parsers.excel.ExcelParser;
import com.rbbs.scrapper.parsers.excel.model.FinalWorkbook;
import com.rbbs.scrapper.parsers.site.SiteParser;
import com.rbbs.scrapper.parsers.site.model.Site;
import com.rbbs.scrapper.parsers.site.model.Tender;
import com.rbbs.scrapper.parsers.util.ExcelCriteria;
import com.rbbs.scrapper.parsers.util.SiteSearchCriteria;
import com.rbbs.scrapper.shared.LogMessages;
import com.rbbs.scrapper.shared.LoggerHelper;
import com.rbbs.scrapper.shared.ResourcesUtil;
import com.rbbs.scrapper.util.file_storages.WindowsFileStorage;
import com.rbbs.scrapper.util.file_storages.interfaces.FileStorage;
import com.rbbs.scrapper.util.generate_reports.SiteReport;
import com.rbbs.scrapper.util.generate_reports.TenderReport;
import javafx.application.Platform;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.rbbs.scrapper.shared.LogMessages.MainProcessor.*;

public class MainProcessor {
    private static final Logger consoleLogger = Logger.getLogger("APP1");
    private static final Logger fileLogger = Logger.getLogger("APP2");
    private final AtomicBoolean isRunning = new AtomicBoolean(false);

    private ExcelCriteria excelCriteria;
    private SiteParser siteParser;

    private static final List<String> extensions = new ArrayList<String>(Arrays.asList(".xls", ".xlsx",".XLS", ".XLSX"));
    //TODO Добавить расширения, капсом
    private static final List<String> allExtensions = new ArrayList<>(Arrays.asList(".xls", ".xlsx", ".pdf", ".rar", ".zip", ".7z",".XLS", ".XLSX", ".PDF", ".RAR", ".ZIP", ".7Z"));

    private static final String WORK_DIRECTORY_NAME = "\\Документы_закупки";
    private static final String REPORT_FIRST_PART_NAME = "Отчет_";

    private int searchStringCount;
    private int tendersCount;
    private int filesCount;
    private int excelFilesCount;
    private int reportsFilesCount;

    public MainProcessor() {
        this.isRunning.set(true);
    }

    public int getSearchStringCount() {
        return searchStringCount;
    }

    public int getTendersCount() {
        return tendersCount;
    }

    public int getFilesCount() {
        return filesCount;
    }

    public int getExcelFilesCount() {
        return excelFilesCount;
    }

    public int getReportsFilesCount() {
        return reportsFilesCount;
    }

    public void offlineStart(String pathToDir) {
        LoggerHelper.info(INFO_STARTING_OFFLINE_MODE, fileLogger, consoleLogger);

        searchStringCount = 0;
        tendersCount = 0;
        filesCount = 0;
        excelFilesCount = 0;
        reportsFilesCount = 0;

        initExcelCriteria();
        Tender tender = new Tender();
        tender.setDirectory(new File(pathToDir));
        prepareTender(tender);
        processTender(tender, excelCriteria);
    }

    public void onlineStart(String pathToDirectory,
                            String searchStrings, String sortsStrings,
                            boolean searchBy44, boolean searchBy223,
                            String publishDateFrom, String publishDateTo,
                            String applicationDateFrom, String applicationDateTo) {
        LoggerHelper.info(INFO_STARTING_ONLINE_MODE, fileLogger, consoleLogger);

        searchStringCount = 0;
        tendersCount = 0;
        filesCount = 0;
        excelFilesCount = 0;
        reportsFilesCount = 0;

        initExcelCriteria();
        if (isRunning.get()) {
            SiteSearchCriteria siteSearchCriteria = new SiteSearchCriteria.Builder()
                    .setSearchLineCriteria(formatterArgsSearchCriteria(searchStrings))
                    .setSortsStrings(sortsStrings)
                    .setSearchBy44(searchBy44)
                    .setSearchBy223(searchBy223)
                    .setPublishDateFrom(publishDateFrom)
                    .setPublishDateTo(publishDateTo)
                    .setApplicationDateFrom(applicationDateFrom)
                    .setApplicationDateTo(applicationDateTo)
                    .build();

            siteParser = new SiteParser(siteSearchCriteria, pathToDirectory);
            Site site = siteParser.getSite();
            siteParser.close();

            for (Tender tender : site.getTenders()) {
                if (isRunning.get()) {
                    LoggerHelper.info(INFO_SELECTED_TENDER_PART_1 +
                            tender.getLinkToTender() + INFO_SELECTED_TENDER_PART_2 +
                            tender.getDirectory().getAbsolutePath(), fileLogger, consoleLogger);

                    LoggerHelper.info(INFO_START_PREPARING_TENDER, fileLogger, consoleLogger);
                    prepareTender(tender);
                    LoggerHelper.info(INFO_FINISHED_PREPARING_TENDER, fileLogger, consoleLogger);

                    LoggerHelper.info(INFO_START_PROCESSING_TENDER, fileLogger, consoleLogger);
                    processTender(tender, excelCriteria);
                    LoggerHelper.info(INFO_FINISHED_PROCESSING_TENDER, fileLogger, consoleLogger);

                    LoggerHelper.info(INFO_START_CREATING_REPORT, fileLogger, consoleLogger);
                    TenderReport.createReport(tender);
                    LoggerHelper.info(INFO_FINISHED_CREATING_REPORT, fileLogger, consoleLogger);
                    tendersCount++;
                } else return;
            }
            if (isRunning.get()) {
                SiteReport.createReport(site, pathToDirectory);
            }
        }
    }

    private void initExcelCriteria() {
        if (isRunning.get()) {
            LoggerHelper.info(INFO_START_EXCEL_CRITERIA_INITIALIZING, fileLogger, consoleLogger);
            try {
                this.excelCriteria = new ExcelCriteria(
                        ResourcesUtil.ExcelCriteriaPaths.EXTERNAL_ROW_CRITERIA_FILE_PATH,
                        ResourcesUtil.ExcelCriteriaPaths.EXTERNAL_TABLE_BODY_CRITERIA_FILE_PATH,
                        ResourcesUtil.ExcelCriteriaPaths.EXTERNAL_MULTILINE_CRITERIA_FILE_PATH);
                LoggerHelper.info(INFO_FINISHED_EXCEL_CRITERIA_INITIALIZING, fileLogger, consoleLogger);
            } catch (Exception e) {
                LoggerHelper.error(ERROR_FAILED_EXCEL_CRITERIA_INITIALIZING, consoleLogger, fileLogger);
                fileLogger.error(e);
                isRunning.set(false);
            }

        } else return;
    }

    @NotNull
    private List<String> formatterArgsSearchCriteria(@NotNull String searchCriteria) {
        List<String> resultList = new ArrayList<>();

        String[] list = searchCriteria.split(";");
        for (String s : list) {
            resultList.add(s.trim().replaceAll(" ", "+"));
        }
        searchStringCount = resultList.size();
        return resultList;
    }

    private void prepareTender(@NotNull Tender tender) {
        if (isRunning.get()) {
            FileStorage windowsFileStorage = new WindowsFileStorage();
            File directory = windowsFileStorage.createDirectory(tender.getDirectory().getAbsolutePath(), WORK_DIRECTORY_NAME);

            windowsFileStorage.unarchiveAllRecursive(directory, directory);
        } else return;
    }

    private boolean processTender(@NotNull Tender tender, ExcelCriteria excelCriteria) {
        if (isRunning.get()) {
            FileStorage windowsFileStorage = new WindowsFileStorage();

            File directory = new File(tender.getDirectory().getAbsolutePath() + WORK_DIRECTORY_NAME);
            try {
                windowsFileStorage.moveFiles(directory, directory);
            } catch (IOException e) {
                LoggerHelper.error(ERROR_COPYING_FILE_, fileLogger, consoleLogger);
            }
            List<File> allTendersFilesNames = windowsFileStorage.getFilesInDirectory(directory);

            if (allTendersFilesNames.size() > 0) {
                filesCount += allTendersFilesNames.size();
                createReports(tender, allTendersFilesNames, excelCriteria);
                return true;
            } else {
                LoggerHelper.info(ERROR_FILE_NOT_FOUND, fileLogger, consoleLogger);
                return false;
            }
        }
        return false;
    }

    private void createReport(Tender tender, File file, FileStorage fileStorage, ExcelParser excelParser) {
        LoggerHelper.info(INFO_CHECKING_EXTENSION_FILE + file.getAbsolutePath(), consoleLogger, fileLogger);
        if (fileStorage.checkFileExtension(file, extensions)) {
            LoggerHelper.info(INFO_THIS_IS_AN_EXCEL_FILE_CHECKING_IF_IT_IS_AN_ESTIMATE, consoleLogger, fileLogger);

            FinalWorkbook finalWorkbook = excelParser.getReport(file);
            excelFilesCount++;

            if (finalWorkbook != null) {
                LoggerHelper.info(INFO_THIS_FILE_IS_AN_ESTIMATE + file.getAbsolutePath(), consoleLogger, fileLogger);

                File report = fileStorage.createFile(file.getParentFile().getParentFile().getAbsolutePath(), REPORT_FIRST_PART_NAME + tender.getTenderNumber() + "_" + file.getName());
                excelParser.writeInFile(report, fileStorage.getFileExtension(file), finalWorkbook);

                LoggerHelper.info(INFO_GENERATING_REPORT + file.getAbsolutePath(), consoleLogger, fileLogger);

                reportsFilesCount++;
            } else {
                LoggerHelper.info(INFO_THIS_FILE_IS_NOT_AN_ESTIMATE, consoleLogger, fileLogger);
            }
        } else if (!fileStorage.checkFileExtension(file, allExtensions)) {
            LoggerHelper.info(INFO_FILE_EXTENSION_DOES_NOT_MATCHING, consoleLogger, fileLogger);
            Platform.runLater(() -> {
                if (!file.isDirectory()) {
                    LoggerHelper.info(INFO_REMOVE_FILE + file.getAbsolutePath(), consoleLogger, fileLogger);
                    fileStorage.removeFile(file);
                } /*else {
                    LoggerHelper.info("Remove directory: \n" + file.getAbsolutePath(), consoleLogger, fileLogger);
                    fileStorage.removeDirectory(file);
                }*/
            });
        }
    }

    private void createReports(Tender tender, @NotNull List<File> allTendersFilesNames, ExcelCriteria excelCriteria) {
        FileStorage fileStorage = new WindowsFileStorage();
        ExcelParser excelParser = new ExcelParser(excelCriteria);
        for (File file : allTendersFilesNames) {
            if (isRunning.get()) {
                createReport(tender, file, fileStorage, excelParser);
            } else return;
        }
    }

    public void terminate() {
        isRunning.set(false);
        if (siteParser != null) {
            siteParser.terminate();
        }
    }

    public void start() {
        isRunning.set(true);
    }
}
