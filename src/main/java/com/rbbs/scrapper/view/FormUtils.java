package com.rbbs.scrapper.view;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import org.apache.log4j.Logger;

public class FormUtils {
    private static final Logger logConsole = Logger.getLogger("APP1");
    private static final Logger logFile = Logger.getLogger("APP2");

    public static void showAlertInformation(String title, String headerText, String logg) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.showAndWait().ifPresent(rs -> {
            if (rs == ButtonType.OK) {
                logFile.info(logg);
            }
        });
    }

    public static void showAlertWarning(String title, String headerText, String logg) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.showAndWait().ifPresent(rs -> {
            if (rs == ButtonType.OK) {
                logFile.info(logg);
            }
        });
    }

    public static void showAlertError(String title, String headerText, String logg) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.showAndWait().ifPresent(rs -> {
            if (rs == ButtonType.OK) {
                logFile.info(logg);
            }
        });
    }

    public static void showAlertConfiguration(String title, String headerText, String logg) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.showAndWait().ifPresent(rs -> {
            if (rs == ButtonType.OK) {
                logFile.info(logg);
            }
        });
    }
}
