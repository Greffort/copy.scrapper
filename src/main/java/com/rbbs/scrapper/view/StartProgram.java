package com.rbbs.scrapper.view;

import com.rbbs.scrapper.shared.LoggerHelper;
import com.rbbs.scrapper.shared.ResourcesUtil;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import static com.rbbs.scrapper.util.MomKiller.killProcess;
import static com.rbbs.scrapper.view.ViewConstants.MAIN_FORM_PATH;

public class StartProgram extends Application {
    private static final Logger logConsole = Logger.getLogger("APP1");
    private static final Logger logFile = Logger.getLogger("APP2");

    public static void main(final String[] args) {
        launch(args);
    }

    @Override
    public void start(final Stage primaryStage) {

        try {
            ResourcesUtil.createWebDriverFile();
            ResourcesUtil.createExcelCriteriaFiles();

            final Parent root = FXMLLoader.load(getClass().getResource(MAIN_FORM_PATH));
            final Scene scene = new Scene(root);
            int width = 460;
            int height = 690;
            primaryStage.minHeightProperty().setValue(height);
            primaryStage.minWidthProperty().setValue(width);
            primaryStage.setHeight(height);
            primaryStage.setWidth(width);
            primaryStage.maxHeightProperty().setValue(height);
            primaryStage.maxWidthProperty().setValue(width);

            String APPLICATION_NAME = "RBBS Scrapper 1.0.0";
            primaryStage.setTitle(APPLICATION_NAME);
            primaryStage.setScene(scene);
            logFile.info("********** START APP ********** ");
            logConsole.info("********** START APP ********** ");
            primaryStage.show();
            primaryStage.setOnCloseRequest(event -> {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("RBBS Scrapper");
                alert.setHeaderText("Вы уверены, что хотите выйти?");
                alert.showAndWait().ifPresent(rs -> {
                    if (rs == ButtonType.OK) {
                        killProcess(ResourcesUtil.WebDriverPaths.FILE_NAME);
                        LoggerHelper.info("Завершение работы программы. Пожалуйста подождите - процесс может занять некоторое время.", logFile, logConsole);
                        logFile.info("Приложение закрыто");
                        System.exit(0);
                    }
                    if (rs == ButtonType.CANCEL) {
                        logFile.info("Отмена закрытия приложения");
                        event.consume();
                    }
                });
            });
        } catch (Exception e) {
            logFile.error(e);
        }
    }
}
