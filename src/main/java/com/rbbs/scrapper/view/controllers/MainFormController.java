package com.rbbs.scrapper.view.controllers;

import com.rbbs.scrapper.controllers.MainProcessor;
import com.rbbs.scrapper.shared.LoggerHelper;
import com.rbbs.scrapper.view.FormUtils;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class MainFormController {
    private static final Logger consoleLogger = Logger.getLogger("APP1");
    private static final Logger fileLogger = Logger.getLogger("APP2");

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.getDefault());
    private StringConverter<LocalDate> converter = new StringConverter<LocalDate>() {
        @Override
        public String toString(LocalDate date) {
            if (date != null) {
                return formatter.format(date);
            } else {
                return StringUtils.EMPTY;
            }
        }

        @Override
        public LocalDate fromString(String string) {
            if (string != null && !string.isEmpty()) {
                return LocalDate.parse(string, formatter);
            } else {
                return null;
            }
        }
    };

    @FXML
    private ProgressIndicator spinner;

    //Labels
    @FXML
    private Label labelCountSourceFiles;
    @FXML
    private Label labelCountExcelFiles;
    @FXML
    private Label labelCountGeneratedFiles;
    @FXML
    private Label labelTime;
    @FXML
    private Label labelCountSearchString;
    @FXML
    private Label labelCountTenders;

    //Text fields
    @FXML
    private TextField dirPathTextField;
    @FXML
    private TextArea searchLineTextArea;

    // FZ Radio Buttons
    //TODO Заменить радио-кнопки чекбоксами?
    // - Думаю нет, радиобаттон здесь конечно реализован неверно, но чекбокс - можем брать несколько значений, а радтиобатон - только одно (подразумевается)
    @FXML
    private RadioButton isSearchBy44RadioButton;
    @FXML
    private RadioButton isSearchBy223RadioButton;

    //Online or Offline mode Radio Buttons
    @FXML
    private RadioButton isOfflineRadioButton;

    //Date Pickers
    @FXML
    private DatePicker publishDateFromPicker;
    @FXML
    private DatePicker publishDateToPicker;
    @FXML
    private DatePicker applicationDateFromPicker;
    @FXML
    private DatePicker applicationDateToPicker;

    //Buttons
    @FXML
    private Button runButton;
    @FXML
    private Button stopButton;
    @FXML
    private Button changeDirButton;

    private Thread thread;

    private MainProcessor mainProcessor = new MainProcessor();


    @FXML
    private void initialize() {
        runButton.setDisable(false);
        stopButton.setDisable(true);
        changeDirButton.setDisable(false);
        spinner.setVisible(false);

        resetLabelText();

        publishDateFromPicker.setConverter(converter);
        publishDateToPicker.setConverter(converter);
        applicationDateFromPicker.setConverter(converter);
        applicationDateToPicker.setConverter(converter);

        isSearchBy44RadioButton.setSelected(true);

        applicationDateFromPicker.setValue(LocalDate.now());
//        isSearchBy223RadioButton.setVisible(false);
    }

    @FXML
    void radioButton223() {
        isSearchBy44RadioButton.setSelected(false);
        isOfflineRadioButton.setSelected(false);
    }

    @FXML
    void radioButton44() {
        isSearchBy223RadioButton.setSelected(false);
        isOfflineRadioButton.setSelected(false);
    }

    @FXML
    void radioButtonOffline() {
        isSearchBy223RadioButton.setSelected(false);
        isSearchBy44RadioButton.setSelected(false);
    }

    @FXML
    public void buttonChangeDirectory() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDirectory = directoryChooser.showDialog(new Stage());

        if (selectedDirectory != null) {
            dirPathTextField.setText(selectedDirectory.getAbsolutePath());
        } else {
            FormUtils.showAlertConfiguration("RBBS Scrapper", "Директория не выбрана", "Директория не выбрана");
        }
    }

    private String getFormattedDate(LocalDate date) {
        return date != null ? date.format(formatter) : StringUtils.EMPTY;
    }

    private void resetLabelText() {
        labelCountSearchString.setText(StringUtils.EMPTY);
        labelCountTenders.setText(StringUtils.EMPTY);
        labelCountSourceFiles.setText(StringUtils.EMPTY);
        labelCountExcelFiles.setText(StringUtils.EMPTY);
        labelCountGeneratedFiles.setText(StringUtils.EMPTY);
        labelTime.setText(StringUtils.EMPTY);
    }

    private boolean checkSearchConfiguration() {
        boolean isDirectoryFilled = !dirPathTextField.getText().isEmpty();
        boolean isOfflineMode = isOfflineRadioButton.isSelected();
        boolean isSearchLineFilled = !searchLineTextArea.getText().isEmpty();

        boolean isPublishDateFromFilled = publishDateFromPicker.getValue() != null;
        boolean isPublishDateToFilled = publishDateToPicker.getValue() != null;
        boolean isApplicationDateFromFilled = applicationDateFromPicker.getValue() != null;
        boolean isApplicationDateToFilled = applicationDateToPicker.getValue() != null;
        boolean isAnyDateFilled = isPublishDateFromFilled || isPublishDateToFilled || isApplicationDateFromFilled || isApplicationDateToFilled;

        if (isDirectoryFilled) {
            if (isOfflineMode) {
                return true;
            } else {
                if (isSearchLineFilled) {
                    if (isAnyDateFilled) return true;
                    else {
                        FormUtils.showAlertConfiguration("RBBS Scrapper", "Необходимо указать дату размещения и/или дату окончания подачи заявок", "");
                        return false;
                    }
                } else {
                    FormUtils.showAlertConfiguration("RBBS Scrapper", "Необходимо указать поисковые запросы разделив их ';'\nНапример:\nКапитальный ремонт; Ремонт дорог; Строительство;", "");
                    return false;
                }
            }
        } else {
            FormUtils.showAlertConfiguration("RBBS Scrapper", "Необходимо указать папку для хранения отчетов по закупкам.", "");
            return false;
        }
    }

    @FXML
    public void runButtonClicked() {
        mainProcessor.start();
        runButton.setDisable(true);
        stopButton.setDisable(false);
        spinner.setVisible(true);
        resetLabelText();
        long startTime = System.currentTimeMillis();

        if (checkSearchConfiguration()) {
            LoggerHelper.info("Start processing ...\n", fileLogger, consoleLogger);

            this.thread = new Thread(() -> {
                spinner.setVisible(true);

                boolean isOfflineMode = isOfflineRadioButton.isSelected();

                String pathToDirectory = dirPathTextField.getText();


                if (isOfflineMode) {
                    mainProcessor.offlineStart(pathToDirectory);
                } else {
                    boolean searchBy44 = isSearchBy44RadioButton.isSelected();
                    boolean searchBy223 = isSearchBy223RadioButton.isSelected();
                    String publishDateFromLine = getFormattedDate(publishDateFromPicker.getValue());
                    String publishDateToLine = getFormattedDate(publishDateToPicker.getValue());
                    String applicationDateFrom = getFormattedDate(applicationDateFromPicker.getValue());
                    String applicationDateTo = getFormattedDate(applicationDateToPicker.getValue());
                    String searchStrings = searchLineTextArea.getText();
                    final String sortsStrings = "PRICE";

                    mainProcessor.onlineStart(pathToDirectory,
                            searchStrings, sortsStrings,
                            searchBy44, searchBy223,
                            publishDateFromLine, publishDateToLine,
                            applicationDateFrom, applicationDateTo);
                }

                Platform.runLater(() -> {
                    stopButton.setDisable(true);
                    spinner.setVisible(false);
                    runButton.setDisable(false);
                    labelCountSearchString.setText("Поисковых запросов - " + mainProcessor.getSearchStringCount());
                    labelCountTenders.setText("Обработано закупок - " + mainProcessor.getTendersCount());
                    labelCountSourceFiles.setText("Обработанных файлов - " + mainProcessor.getFilesCount());
                    labelCountExcelFiles.setText("Обработанных .xls файлов - " + mainProcessor.getExcelFilesCount());
                    labelCountGeneratedFiles.setText("Сгенерированно отчетов -  " + mainProcessor.getReportsFilesCount());
                    long endTime = System.currentTimeMillis();
                    labelTime.setText("Total execution time: " + (endTime - startTime) / 1000 + "s" + "/" + (endTime - startTime) / 60000 + "min");
                });

                Platform.runLater(() -> {
                    LoggerHelper.info("***** PROCESSING COMPLETED SUCCESSFULLY *****", fileLogger, consoleLogger);
                    FormUtils.showAlertInformation("RBBS Scrapper", "Обработка завершена", "Парсинг данных завершен");
                });
            });
            thread.setPriority(1);
            thread.start();
        } else {
            spinner.setVisible(false);
            runButton.setDisable(false);
            stopButton.setDisable(true);
        }
    }

    @FXML
    public void stopButtonClicked(ActionEvent actionEvent) {
        mainProcessor.terminate();
        spinner.setVisible(false);
        LoggerHelper.info("Завершение работы программы. Пожалуйста подождите - процесс может занять некоторое время.", fileLogger, consoleLogger);
    }
}
