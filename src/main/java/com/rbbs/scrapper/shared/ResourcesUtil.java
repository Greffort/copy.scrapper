package com.rbbs.scrapper.shared;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import static com.rbbs.scrapper.shared.LogMessages.ResourcesUtil.ERROR_DURING_EXCEL_CRITERIA_FILES_CREATION;
import static com.rbbs.scrapper.shared.LogMessages.ResourcesUtil.ERROR_DURING_WEB_DRIVER_FILE_CREATION;


public class ResourcesUtil {

    private static final Logger fileLogger = Logger.getLogger("APP2");

    public static final class WebDriverPaths {

        //External
        private static final String EXTERNAL_DIRECTORY_PATH = "system/webDriver";
        public static final String FILE_NAME = "RBBSChromeDriver.exe";
        public static final String EXTERNAL_FILE_PATH = EXTERNAL_DIRECTORY_PATH + "/" + FILE_NAME;

        //Internal
        private static final String RESOURCE_FILE_PATH = "webDriver/RBBSChromeDriver.exe";
    }

    public static final class ExcelCriteriaPaths {
        //External
        private static final String EXTERNAL_DIRECTORY_PATH = "system/excelCriteria";
        private static final String ROW_CRITERIA_FILE_NAME = "CriterionForRowIndexes.xlsx";
        private static final String TABLE_BODY_CRITERIA_FILE_NAME = "CriterionForTableBody.xlsx";
        private static final String MULTILINE_CRITERIA_FILE_NAME = "CriterionForMultilineSelectedRow.xlsx";
        public static final String EXTERNAL_ROW_CRITERIA_FILE_PATH = EXTERNAL_DIRECTORY_PATH + "/" + ROW_CRITERIA_FILE_NAME;
        public static final String EXTERNAL_TABLE_BODY_CRITERIA_FILE_PATH = EXTERNAL_DIRECTORY_PATH + "/" + TABLE_BODY_CRITERIA_FILE_NAME;
        public static final String EXTERNAL_MULTILINE_CRITERIA_FILE_PATH = EXTERNAL_DIRECTORY_PATH + "/" + MULTILINE_CRITERIA_FILE_NAME;

        //Internal
        private static final String RESOURCE_ROW_CRITERIA_FILE_PATH = "excelCriteria/CriterionForRowIndexes.xlsx";
        private static final String RESOURCE_TABLE_BODY_CRITERIA_FILE_PATH = "excelCriteria/CriterionForTableBody.xlsx";
        private static final String RESOURCE_MULTILINE_CRITERIA_FILE_PATH = "excelCriteria/CriterionForMultilineSelectedRow.xlsx";
    }

    public static void createWebDriverFile() {
        File dir = new File(WebDriverPaths.EXTERNAL_DIRECTORY_PATH);
        File driver = new File(WebDriverPaths.EXTERNAL_FILE_PATH);
        try {
            if (!dir.exists()) {
                dir.mkdirs();
                createFile(WebDriverPaths.RESOURCE_FILE_PATH, driver);
            } else {
                if (!driver.exists()) {
                    createFile(WebDriverPaths.RESOURCE_FILE_PATH, driver);
                }
            }
        } catch (IOException e) {
            LoggerHelper.error(ERROR_DURING_WEB_DRIVER_FILE_CREATION, e, fileLogger);
        }
    }

    private static void createFile(String resourcePath, File file) throws IOException {
        try (InputStream inputStream = ResourcesUtil.class.getClassLoader().getResourceAsStream(resourcePath)) {
            Files.copy(inputStream, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
    }

    public static void createExcelCriteriaFiles() {
        File dir = new File(ExcelCriteriaPaths.EXTERNAL_DIRECTORY_PATH);
        File multilineCriteriaFile = new File(ExcelCriteriaPaths.EXTERNAL_MULTILINE_CRITERIA_FILE_PATH);
        File rowCriteriaFile = new File(ExcelCriteriaPaths.EXTERNAL_ROW_CRITERIA_FILE_PATH);
        File tableBodyCriteriaFile = new File(ExcelCriteriaPaths.EXTERNAL_TABLE_BODY_CRITERIA_FILE_PATH);

        try {
            if (!dir.exists()) {
                dir.mkdirs();
                createFile(ExcelCriteriaPaths.RESOURCE_MULTILINE_CRITERIA_FILE_PATH, multilineCriteriaFile);
                createFile(ExcelCriteriaPaths.RESOURCE_ROW_CRITERIA_FILE_PATH, rowCriteriaFile);
                createFile(ExcelCriteriaPaths.RESOURCE_TABLE_BODY_CRITERIA_FILE_PATH, tableBodyCriteriaFile);

            } else {
                if (!multilineCriteriaFile.exists()) {
                    createFile(ExcelCriteriaPaths.RESOURCE_MULTILINE_CRITERIA_FILE_PATH, multilineCriteriaFile);
                }

                if (!rowCriteriaFile.exists()) {
                    createFile(ExcelCriteriaPaths.RESOURCE_ROW_CRITERIA_FILE_PATH, rowCriteriaFile);
                }

                if (!tableBodyCriteriaFile.exists()) {
                    createFile(ExcelCriteriaPaths.RESOURCE_TABLE_BODY_CRITERIA_FILE_PATH, tableBodyCriteriaFile);
                }
            }
        } catch (IOException e) {
            LoggerHelper.error(ERROR_DURING_EXCEL_CRITERIA_FILES_CREATION, e, fileLogger);
        }
    }
}
