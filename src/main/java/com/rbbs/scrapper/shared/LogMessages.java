package com.rbbs.scrapper.shared;

public class LogMessages {
    public static final class ResourcesUtil {
        public static final String ERROR_DURING_EXCEL_CRITERIA_FILES_CREATION = "Error during creation excel criteria files";
        public static final String ERROR_DURING_WEB_DRIVER_FILE_CREATION = "Error during web driver creation";
    }

    public static final class MainProcessor {
        public static final String INFO_STARTING_OFFLINE_MODE = "Starting in offline mode...";
        public static final String INFO_STARTING_ONLINE_MODE = "Starting in online mode...";
        public static final String INFO_SELECTED_TENDER_PART_1 = "\nSelected tender: \nLINK:";
        public static final String INFO_SELECTED_TENDER_PART_2 = "\nPATH:";
        public static final String INFO_START_PREPARING_TENDER = "Preparing tender...";
        public static final String INFO_FINISHED_PREPARING_TENDER = "Preparing tender successfully completed.\n";
        public static final String INFO_START_PROCESSING_TENDER = "Processing tender...";
        public static final String INFO_FINISHED_PROCESSING_TENDER = "Processing tender successfully completed.\n";
        public static final String INFO_START_CREATING_REPORT = "Creating report ...";
        public static final String INFO_FINISHED_CREATING_REPORT = "Creating report successfully completed.\n";
        public static final String INFO_START_EXCEL_CRITERIA_INITIALIZING = "Excel criteria initialization started...";
        public static final String INFO_FINISHED_EXCEL_CRITERIA_INITIALIZING = "Excel criteria initialization finished";
        public static final String INFO_CHECKING_EXTENSION_FILE = "Checking extension file: ";
        public static final String INFO_THIS_IS_AN_EXCEL_FILE_CHECKING_IF_IT_IS_AN_ESTIMATE = "This is an excel file. Checking if it is an estimate ...";
        public static final String INFO_THIS_FILE_IS_AN_ESTIMATE = "This file is an estimate...";
        public static final String INFO_GENERATING_REPORT = "Generating report: ";
        public static final String INFO_THIS_FILE_IS_NOT_AN_ESTIMATE = "This file is not an estimate\n";
        public static final String INFO_FILE_EXTENSION_DOES_NOT_MATCHING = "File extension does not matching\n";
        public static final String INFO_REMOVE_FILE = "Remove file: \n";

        public static final String ERROR_FAILED_EXCEL_CRITERIA_INITIALIZING = "Excel criteria initialization failed";
        public static final String ERROR_COPYING_FILE_ = "Error copying files";
        public static final String ERROR_FILE_NOT_FOUND = "Files not found";
    }

    public static final class ChromeDriver {
        public static final String INFO_CREATING_WEB_DRIVER = "Creating WebDriver ...";
        public static final String INFO_SUCCESS_CREATING_WEB_DRIVER = "Google Web Driver successfully created";
        public static final String INFO_FAILED_CREATING_WEB_DRIVER = "FAILED during driver creating";
    }

    public static final class SiteParser {
        public static final String INFO_PARSING_TENDER_LINK = "Parsing tender link - ";
        public static final String INFO_PARSING_TENDER = "Parsing tender - ";
        public static final String INFO_SUCCESS = "SUCCESS\n";
        public static final String INFO_DOWNLOADING_FILE = "Downloading file - ";
        public static final String INFO_SKIPPING_FILE = "Skipping file - ";
        public static final String INFO_SITE_PARSER_TEMINATED = "Site parser terminated";

        public static final String ERROR_SUBSTRING = "Ошибка выделение подстроки";
        public static final String ERROR_DOWNLOADING_FILE = "Downloading file - FAILED\n";
        public static final String ERROR_DOWNLOADING_WEB_PAGE = "Ошибка загрузки HTML страницы - ";
    }
}
