package com.rbbs.scrapper.parsers.excel.model;


import org.apache.poi.ss.usermodel.Row;

import java.util.ArrayList;
import java.util.List;

public class FinalSheet {

    private List<FinalObject> finalObjects;

    private int indexRow1;
    private int indexRow2 = 1488;
    private int indexRow3 = 1488;
    private int indexRow4 = 1488;
    private int indexRow5 = 1488;
    private int indexRow6 = 1488;
    private int indexRow7;

    private boolean isSmeta = false;

    private int numberRowFromToStartTheSearch;

    private int numberCellFromSearchCriterion;

    private List<StringBuilder> allRowsInStringBuilder = new ArrayList<>();

    private List<Integer> numberSelectedRows = new ArrayList<>();

    private List<Row> selectedRows = new ArrayList<>();

    public List<FinalObject> getFinalObjects() {
        return finalObjects;
    }

    public void setFinalObjects(List<FinalObject> finalObjects) {
        this.finalObjects = finalObjects;
    }

    public int getIndexRow1() {
        return indexRow1;
    }

    public void setIndexRow1(int indexRow1) {
        this.indexRow1 = indexRow1;
    }

    public int getIndexRow2() {
        return indexRow2;
    }

    public void setIndexRow2(int indexRow2) {
        this.indexRow2 = indexRow2;
    }

    public int getIndexRow3() {
        return indexRow3;
    }

    public void setIndexRow3(int indexRow3) {
        this.indexRow3 = indexRow3;
    }

    public int getIndexRow4() {
        return indexRow4;
    }

    public void setIndexRow4(int indexRow4) {
        this.indexRow4 = indexRow4;
    }

    public int getIndexRow5() {
        return indexRow5;
    }

    public void setIndexRow5(int indexRow5) {
        this.indexRow5 = indexRow5;
    }

    public int getIndexRow6() {
        return indexRow6;
    }

    public void setIndexRow6(int indexRow6) {
        this.indexRow6 = indexRow6;
    }

    public int getIndexRow7() {
        return indexRow7;
    }

    public void setIndexRow7(int indexRow7) {
        this.indexRow7 = indexRow7;
    }

    public boolean isSmeta() {
        return isSmeta;
    }

    public void setSmeta(boolean smeta) {
        isSmeta = smeta;
    }

    public int getNumberRowFromToStartTheSearch() {
        return numberRowFromToStartTheSearch;
    }

    public void setNumberRowFromToStartTheSearch(int numberRowFromToStartTheSearch) {
        this.numberRowFromToStartTheSearch = numberRowFromToStartTheSearch;
    }

    public int getNumberCellFromSearchCriterion() {
        return numberCellFromSearchCriterion;
    }

    public void setNumberCellFromSearchCriterion(int numberCellFromSearchCriterion) {
        this.numberCellFromSearchCriterion = numberCellFromSearchCriterion;
    }

    public List<StringBuilder> getAllRowsInStringBuilder() {
        return allRowsInStringBuilder;
    }

    public void setAllRowsInStringBuilder(List<StringBuilder> allRowsInStringBuilder) {
        this.allRowsInStringBuilder = allRowsInStringBuilder;
    }

    public List<Integer> getNumberSelectedRows() {
        return numberSelectedRows;
    }

    public void setNumberSelectedRows(List<Integer> numberSelectedRows) {
        this.numberSelectedRows = numberSelectedRows;
    }

    public List<Row> getSelectedRows() {
        return selectedRows;
    }

    public void setSelectedRows(List<Row> selectedRows) {
        this.selectedRows = selectedRows;
    }
}
