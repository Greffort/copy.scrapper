package com.rbbs.scrapper.parsers.excel.model;

import java.util.List;

public class FinalWorkbook {

    private List<FinalSheet> sheets;

    public FinalWorkbook(List<FinalSheet> sheets) {
        this.sheets = sheets;
    }

    public List<FinalSheet> getSheets() {
        return sheets;
    }

    public void setSheets(List<FinalSheet> sheets) {
        this.sheets = sheets;
    }
}
