package com.rbbs.scrapper.parsers.excel;

import com.rbbs.scrapper.parsers.excel.model.FinalObject;
import com.rbbs.scrapper.parsers.excel.model.FinalSheet;
import com.rbbs.scrapper.parsers.excel.model.FinalWorkbook;
import com.rbbs.scrapper.parsers.util.ExcelCriteria;
import com.rbbs.scrapper.shared.LoggerHelper;
import com.rbbs.scrapper.util.file_storages.WindowsFileStorage;
import com.rbbs.scrapper.util.file_storages.interfaces.FileStorage;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExcelParser {
    private static final Logger logConsole = Logger.getLogger("APP1");
    private static final Logger logFile = Logger.getLogger("APP2");
    private ExcelCriteria excelCriteria;

    public ExcelParser(ExcelCriteria excelCriteria) {
        this.excelCriteria = excelCriteria;
    }

    public FinalWorkbook getReport(File sourceFile) {
        LoggerHelper.info("getReport", logFile);
        FinalWorkbook finalWorkbook = getFinalWorkbook(sourceFile);

        if (finalWorkbook.getSheets().size() > 0) {
            return finalWorkbook;
        } else {
            return null;
        }
    }

    @NotNull
    private FinalWorkbook getFinalWorkbook(File sourceFile) {
        LoggerHelper.info("FinalWorkbook", logFile);
        FileStorage fileStorage = new WindowsFileStorage();
        Workbook book = getWorkbook(sourceFile, fileStorage.getFileExtension(sourceFile));
        List<Sheet> xssfSheets;
        List<FinalSheet> finalSheets = new ArrayList<>();
        if (book != null) {
            xssfSheets = getSheets(book);

            for (Sheet xssfSheet : xssfSheets) {
                FinalSheet finalSheet = new FinalSheet();
                logFile.info("isEstimateTheFirstCheck(xssfSheet, finalSheet) - " + isEstimateTheFirstCheck(xssfSheet, finalSheet));
                if (isEstimateTheFirstCheck(xssfSheet, finalSheet)) {

                    setFinalSheetCellIndexes(xssfSheet, finalSheet);
//                    setSelectedRowsMultiline(xssfSheet, finalSheet);
                    isEstimateCarefulCheck(xssfSheet, finalSheet);
                    logFile.info("finalSheet.getSelectedRows().size() - " + finalSheet.getSelectedRows().size());
                    if (finalSheet.getSelectedRows().size() > 0) {
                        setFinalObjects(finalSheet);

                        changeLine(finalSheet);
                        removeNegativeVolume(finalSheet);
//                        checkUnits(finalSheet);
                        addUpVolume(finalSheet);
                        pasteNumbers(finalSheet);

                        finalSheets.add(finalSheet);
                    }
                }
            }
        }
        return new FinalWorkbook(finalSheets);
    }

    //Это чек шапки
    private boolean isEstimateTheFirstCheck(@NotNull Sheet myExcelSheet, @NotNull FinalSheet finalSheet) {
        LoggerHelper.info("isEstimateTheFirstCheck", logFile);
        finalSheet.setAllRowsInStringBuilder(getRowsInStringBuilder(myExcelSheet));

        List<StringBuilder> stringBuilders = finalSheet.getAllRowsInStringBuilder();
        for (int i = 0; i < stringBuilders.size(); i++) {
            if (checkString(stringBuilders.get(i).toString(), "1.02.03.04.05.0", "12345")) {
                finalSheet.setNumberRowFromToStartTheSearch(i);
                finalSheet.setSmeta(true);
                return true;
            }
        }
        finalSheet.setSmeta(false);
        return false;
    }

    private boolean isEstimateCarefulCheck(@NotNull Sheet myExcelSheet, @NotNull FinalSheet finalSheet) {
        LoggerHelper.info("isEstimateCarefulCheck", logFile);
        finalSheet.setAllRowsInStringBuilder(getRowsInStringBuilder(myExcelSheet));

        List<StringBuilder> stringBuilders = finalSheet.getAllRowsInStringBuilder();
        for (StringBuilder stringBuilder : stringBuilders) {
            if (stringBuilder.toString().toUpperCase().contains("ЛОКАЛЬНАЯ СМЕТА")) {
                setSelectedRowsMultiline(myExcelSheet, finalSheet);
                break;
            } else if (stringBuilder.toString().contains("ЛОКАЛЬНЫЙ РЕСУРСНЫЙ СМЕТНЫЙ РАСЧЁТ")) {
                setSelectedRows(myExcelSheet, finalSheet);
                break;
            } else if (checkString(stringBuilder.toString(), "1.02.03.04.05.0", "12345")) {
//                setSelectedRowsMultiline(myExcelSheet, finalSheet);
                setSelectedRows(myExcelSheet, finalSheet);
                break;
            }
        }
        finalSheet.setSmeta(false);
        return false;
    }

    @NotNull
    private List<StringBuilder> getRowsInStringBuilder(@NotNull Sheet xssfSheet) {
        LoggerHelper.info("getRowsInStringBuilder", logFile);
        List<StringBuilder> allRowsInStringBuilder = new ArrayList<>();

        for (int rowCounter = 0; rowCounter < xssfSheet.getPhysicalNumberOfRows(); rowCounter++) {
            Row row = xssfSheet.getRow(rowCounter);
            StringBuilder stringBuilder = new StringBuilder();
            if (row == null) {
                stringBuilder.append(" | ");
            } else {
                for (int cellCountInRow = 0; cellCountInRow < row.getPhysicalNumberOfCells(); cellCountInRow++) {
                    stringBuilder.append(getCell(row, cellCountInRow));
                }
            }
            allRowsInStringBuilder.add(stringBuilder);
        }
        return allRowsInStringBuilder;
    }

    private void setFinalSheetCellIndexes(@NotNull Sheet myExcelSheet, @NotNull FinalSheet finalSheet) {
        LoggerHelper.info("setFinalSheetCellIndexes", logFile);
        List<String> row2Criterion = excelCriteria.getRow2Criteria();
        List<String> row3Criterion = excelCriteria.getRow3Criteria();
        List<String> row4Criterion = excelCriteria.getRow4Criteria();
        List<String> row5Criterion = excelCriteria.getRow5Criteria();
        List<String> row6Criterion = excelCriteria.getRow6Criteria();

        boolean foundRow2Index = false;
        boolean foundRow3Index = false;
        boolean foundRow4Index = false;
        boolean foundRow5Index = false;
        boolean foundRow6Index = false;

        for (int rowCounter = 0; rowCounter < finalSheet.getNumberRowFromToStartTheSearch() + 1; rowCounter++) {
            Row row = myExcelSheet.getRow(rowCounter);
            if (row != null) {
                for (int cellCountInRow = 0; cellCountInRow < row.getPhysicalNumberOfCells(); cellCountInRow++) {
                    String cell = getCell(row, cellCountInRow);
                    if (!foundRow2Index) {
                        for (String s : row2Criterion) {
                            if (cell.contains(s)) {
                                foundRow2Index = true;
                                finalSheet.setIndexRow2(cellCountInRow);
                                break;
                            }
                        }
                    }
                    if (!foundRow3Index) {
                        for (String s : row3Criterion) {
                            if (cell.contains(s)) {
                                foundRow3Index = true;
                                finalSheet.setIndexRow3(cellCountInRow);
                                break;
                            }
                        }
                    }
                    if (!foundRow4Index) {
                        for (String s : row4Criterion) {
                            if (cell.contains(s)) {
                                foundRow4Index = true;
                                finalSheet.setIndexRow4(cellCountInRow);
                                break;
                            }
                        }
                    }
                    if (!foundRow5Index) {
                        for (String s : row5Criterion) {
                            if (cell.contains(s)) {
                                foundRow5Index = true;
                                finalSheet.setIndexRow5(cellCountInRow);
                                break;
                            }
                        }
                    }
                    if (!foundRow6Index) {
                        for (String s : row6Criterion) {
                            if (cell.contains(s)) {
                                foundRow6Index = true;
                                finalSheet.setIndexRow6(cellCountInRow);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    private void setSelectedRows(@NotNull Sheet myExcelSheet, @NotNull FinalSheet finalSheet) {
        LoggerHelper.info("setSelectedRows", logFile);
        List<StringBuilder> stringBuilders = finalSheet.getAllRowsInStringBuilder();
        List<String> criterionsForBodyTable = excelCriteria.getTableBodyCriteria();

        List<Integer> numberSelectedRows = new ArrayList<>();
        List<Row> selectedRows = new ArrayList<>();

        for (int i = finalSheet.getNumberRowFromToStartTheSearch(); i < stringBuilders.size(); i++) {
            String line = stringBuilders.get(i).toString();
            for (String s : criterionsForBodyTable) {
                Row generalRow = myExcelSheet.getRow(i);
                if (line.contains(s)) {
                    selectedRows.add(generalRow);
                    numberSelectedRows.add(i);
                    break;
                }
            }
        }
        finalSheet.setSelectedRows(selectedRows);
        finalSheet.setNumberSelectedRows(numberSelectedRows);
    }

    private void setSelectedRowsMultiline(@NotNull Sheet myExcelSheet, @NotNull FinalSheet finalSheet) {
        LoggerHelper.info("setSelectedRowsMultiline", logFile);
        List<StringBuilder> stringBuilders = finalSheet.getAllRowsInStringBuilder();
        List<String> criterionsForBodyTable = excelCriteria.getTableBodyCriteria();
        List<String> criterionsForDoubleSelectedRow = excelCriteria.getDoubleSelectedRowCriteria();

        List<Integer> numberSelectedRows = new ArrayList<>();
        List<Row> selectedRows = new ArrayList<>();

        for (int i = finalSheet.getNumberRowFromToStartTheSearch(); i < stringBuilders.size(); i++) {
            String line = stringBuilders.get(i).toString();
            for (String s : criterionsForBodyTable) {
                Row generalRow = myExcelSheet.getRow(i);
                if (line.contains(s)) {
                    selectedRows.add(generalRow);
                    numberSelectedRows.add(i);
//                    Arrays.binarySearch();
                    int indexRow = i + 1;
                    while (true) {
                        Row row = myExcelSheet.getRow(indexRow);
                        StringBuilder stringBuilder = new StringBuilder();

                        if (row != null) {
                            for (int cellCountInRow = 0; cellCountInRow < row.getPhysicalNumberOfCells(); cellCountInRow++) {
                                stringBuilder.append(getCell(row, cellCountInRow));
                            }
                            //ошибка склеивания строк может возникнуть здесь
                            String cell0 = getCell(row, 0);
                            String cell1 = getCell(row, 1);
                            String cell4 = getCell(row, 5);
                            boolean check = false;
                            for (String criterionLine : criterionsForDoubleSelectedRow) {
                                check = stringBuilder.toString().contains(criterionLine);
                                if (check) {
                                    break;
                                }
                            }

                            boolean checkCell1 = cell0.equals("");
                            boolean checkCell2 = cell1.equals("");
                            boolean checkCell3 = cell4.equals("");
                            if (check) {
                                break;
                            } else if ((checkCell1 && checkCell2) || (checkCell1 && checkCell3)) {
                                addRows(generalRow, row);
                            } else {
                                break;
                            }
                        } else {
                            break;
                        }
                        if (indexRow - i > 15) {
                            break;
                        }
                        indexRow++;
                    }
                    break;
                }
            }
        }
        finalSheet.setSelectedRows(selectedRows);
        finalSheet.setNumberSelectedRows(numberSelectedRows);
    }

    private void addRows(@NotNull Row row1, @NotNull Row row2) {
        LoggerHelper.info("addRows", logFile);
        for (int cellCountInRow = 0; cellCountInRow < row1.getPhysicalNumberOfCells(); cellCountInRow++) {
            Cell generalCell = row1.getCell(cellCountInRow);
            String cell1 = getCell(row1, cellCountInRow);
            String cell2 = getCell(row2, cellCountInRow);

            if (generalCell != null) {
                generalCell.setCellValue(cell1 + " " + cell2);
            }
        }
    }

    private void setFinalObjects(@NotNull FinalSheet finalSheet) {
        LoggerHelper.info("setFinalObjects", logFile);
        List<Row> selectedRows = finalSheet.getSelectedRows();
        List<FinalObject> finalObjects = new ArrayList<>();

        finalObjects.add(new FinalObject(
                "№",
                "№ п.п.",
                "Шифр и номер\n позиции норматива",
                "Наименование работ и затрат, единица измерения",
                "Единица измерения",
                "Кол-во единиц",
                "Цена/Стоимость"
        ));

        for (Row row : selectedRows) {
            if (row != null) {
                int index2 = finalSheet.getIndexRow2();
                int index3 = finalSheet.getIndexRow3();
                int index4 = finalSheet.getIndexRow4();
                int index5 = finalSheet.getIndexRow5();
                int index6 = finalSheet.getIndexRow6();

                finalObjects.add(new FinalObject(
                        "",
                        getCell(row, index2),
                        getCell(row, index3),
                        getCell(row, index4),
                        getCell(row, index5),
                        getCell(row, index6),
                        ""
                ));
            }
        }
        finalSheet.setFinalObjects(finalObjects);
    }


    private boolean checkString(String line, String patter1, String patter2) {
        LoggerHelper.info("checkString", logFile);
        Pattern pattern1 = Pattern.compile(patter1);
        Pattern pattern2 = Pattern.compile(patter2);

        Matcher matcher1 = pattern1.matcher(line);
        Matcher matcher2 = pattern2.matcher(line);

        return matcher1.find() || matcher2.find();
    }

    @Nullable
    private Workbook getWorkbook(File file, @NotNull String extension) {
        LoggerHelper.info("getWorkbook", logFile);
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream(file);
            if (extension.toLowerCase().equals("xlsx")) {
                return new XSSFWorkbook(fileInputStream);
            } else if (extension.toLowerCase().equals("xls")) {
                return new HSSFWorkbook(fileInputStream);
            }
        } catch (IOException e) {
            LoggerHelper.error(e, logFile);
            return null;
        }
        return null;
    }

    @NotNull
    private List<Sheet> getSheets(@NotNull Workbook workbook) {
        LoggerHelper.info("getSheets", logFile);
        List<Sheet> xssfSheets = new ArrayList<>();
        int numberOfSheets = workbook.getNumberOfSheets();
        for (int sheetCounter = 0; sheetCounter < numberOfSheets; sheetCounter++) {
            xssfSheets.add(workbook.getSheetAt(sheetCounter));
        }
        return xssfSheets;
    }

    private String getCell(@NotNull Row row, int index) {
        LoggerHelper.info("getCell", logFile);
        Cell cell = row.getCell(index);
        if (cell == null) {
            return "";
        } else {
            return cell.toString();
        }
    }

    public void writeInFile(File file, @NotNull String extension, @NotNull FinalWorkbook finalWorkbook) {
        LoggerHelper.info("writeInFile", logFile);
        Workbook book = new XSSFWorkbook();

        if (extension.toLowerCase().equals("xls")) {
            book = new HSSFWorkbook();
        }

        List<FinalSheet> finalSheets = finalWorkbook.getSheets();
        for (int i = 0; i < finalSheets.size(); i++) {
            FinalSheet finalSheet = finalSheets.get(i);
            List<FinalObject> finalObjects = finalSheet.getFinalObjects();
            String sheetName = "Sheet" + i;
            Sheet sheet = book.createSheet(sheetName);


            for (int rowCounter = 0; rowCounter < finalObjects.size(); rowCounter++) {
                FinalObject finalObject = finalObjects.get(rowCounter);
                Row row = sheet.createRow(rowCounter);

                Cell cell1 = row.createCell(0);
                Cell cell2 = row.createCell(1);
                Cell cell3 = row.createCell(2);
                Cell cell4 = row.createCell(3);
                Cell cell5 = row.createCell(4);
                Cell cell6 = row.createCell(5);
                Cell cell7 = row.createCell(6);

                cell1.setCellValue(finalObject.getCell1());
                cell2.setCellValue(finalObject.getCell2());
                cell3.setCellValue(finalObject.getCell3());
                cell4.setCellValue(finalObject.getCell4());
                cell5.setCellValue(finalObject.getCell5());
                cell6.setCellValue(finalObject.getCell6());
                cell7.setCellValue(finalObject.getCell7());

                sheet.autoSizeColumn(0);
                sheet.autoSizeColumn(1);
                sheet.autoSizeColumn(2);
//                sheet.autoSizeColumn(3);
                sheet.setColumnWidth(3, 14000);
                sheet.autoSizeColumn(4);
                sheet.autoSizeColumn(5);
                sheet.autoSizeColumn(6);
                sheet.autoSizeColumn(7);

                CellStyle style = book.createCellStyle();
                style.setWrapText(true);
                cell1.setCellStyle(style);
                cell2.setCellStyle(style);
                cell3.setCellStyle(style);
                cell4.setCellStyle(style);
                cell5.setCellStyle(style);
                cell6.setCellStyle(style);
                cell7.setCellStyle(style);
            }
        }
        try {
            book.write(new FileOutputStream(file));
            book.close();
        } catch (IOException e) {
            LoggerHelper.error(e, logFile);
        }
    }

    private void removeNegativeVolume(@NotNull FinalSheet finalSheet) {
        LoggerHelper.info("removeNegativeVolume", logFile);
        List<FinalObject> finalObjects = finalSheet.getFinalObjects();

        for (int i = 0; i < finalObjects.size(); i++) {
            FinalObject finalObject = finalObjects.get(i);

            if (finalObject.getCell6().startsWith("-")) {
                finalObjects.remove(finalObject);
                i--;
            }
        }
    }

//    private void checkUnits(@NotNull FinalSheet finalSheet) {
//        log.debug(new Object() {
//        }.getClass().getEnclosingMethod().getName());
//        List<FinalObject> finalObjects = finalSheet.getFinalObjects();
//
//        for (int i = 1; i < finalObjects.size(); i++) {
//            FinalObject finalObject = finalObjects.get(i);
//            String line4 = finalObject.getCell4();
//            String line5 = finalObject.getCell5();
//
//            if (line5.equals("")) {
//                Pattern pattern1 = Pattern.compile("\\s?(\\D\\d|\\D\\D\\D)\\s*$");
//                Matcher matcher1 = pattern1.matcher(line4);
//
//                if (matcher1.find()) {
//                    finalObject.setCell5(matcher1.group());
//                }
//            }
//        }
//    }

    private void changeLine(@NotNull FinalSheet finalSheet) {
        LoggerHelper.info("changeLine", logFile);
        List<FinalObject> finalObjects = finalSheet.getFinalObjects();

        for (int i = 1; i < finalObjects.size(); i++) {
            FinalObject finalObject = finalObjects.get(i);
            String cell = finalObject.getCell6().trim().replace('.', ',');
            finalObject.setCell6(cell);
        }
    }

    // ну нахуй, но работает
    private void addUpVolume(@NotNull FinalSheet finalSheet) {
        LoggerHelper.info("addUpVolume", logFile);
        List<FinalObject> finalObjects = finalSheet.getFinalObjects();

        for (int i = 1; i < finalObjects.size(); i++) {
            FinalObject finalObject = finalObjects.get(i);
            String firstCell3 = finalObject.getCell3().trim();
            String firstCell4 = finalObject.getCell4().trim();

            for (int j = i + 1; j < finalObjects.size(); j++) {
                FinalObject finalObject1 = finalObjects.get(j);
                String secondCell3 = finalObject1.getCell3().trim();
                String secondCell4 = finalObject1.getCell4().trim();

                String number1 = finalObject.getCell6().trim().replace(',', '.');
                String number2 = finalObject1.getCell6().trim().replace(',', '.');

                if (firstCell3.equals(secondCell3) & firstCell4.equals(secondCell4)) {
                    if (!number1.equals("") && !number2.equals("")) {
                        try {
                            BigDecimal first1 = new BigDecimal(number1);
                            BigDecimal second1 = new BigDecimal(number2);

                            BigDecimal result1 = first1.add(second1);

                            String result = String.valueOf(result1).replace('.', ',');

                            finalObject.setCell6(result);
                            finalObjects.remove(finalObject1);
                            j--;
                        } catch (NumberFormatException e) {
                            continue;
                        }
                    }
                }
            }
        }
    }

    private void pasteNumbers(@NotNull FinalSheet finalSheet) {
        LoggerHelper.info("pasteNumbers", logFile);
        List<FinalObject> finalObjects = finalSheet.getFinalObjects();

        for (int i = 1; i < finalObjects.size(); i++) {
            finalObjects.get(i).setCell1(String.valueOf(i));
        }
    }
}
