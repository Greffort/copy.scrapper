package com.rbbs.scrapper.parsers.util;

import java.util.List;

public class SiteSearchCriteria {
    private List<String> searchLineCriteria; // "капитальный ремонт, ремон, строительство".toFormattedArrayList
    private String sortsStrings; //PRICE

    private boolean searchBy44;
    private boolean searchBy223;
    private String publishDateFrom; // Размещение от
    private String publishDateTo; // Размещение до
    private String applicationDateFrom; // Окончание подачи заявок от
    private String applicationDateTo; // Окончание подачи заявок до

    private SiteSearchCriteria() {
    }

    public List<String> getSearchLineCriteria() {
        return searchLineCriteria;
    }

    public void setSearchLineCriteria(List<String> searchLineCriteria) {
        this.searchLineCriteria = searchLineCriteria;
    }

    public String getSortsStrings() {
        return sortsStrings;
    }

    public void setSortsStrings(String sortsStrings) {
        this.sortsStrings = sortsStrings;
    }

    public boolean isSearchBy44() {
        return searchBy44;
    }

    public void setSearchBy44(boolean searchBy44) {
        this.searchBy44 = searchBy44;
    }

    public boolean isSearchBy223() {
        return searchBy223;
    }

    public void setSearchBy223(boolean searchBy223) {
        this.searchBy223 = searchBy223;
    }

    public String getPublishDateFrom() {
        return publishDateFrom;
    }

    public void setPublishDateFrom(String publishDateFrom) {
        this.publishDateFrom = publishDateFrom;
    }

    public String getPublishDateTo() {
        return publishDateTo;
    }

    public void setPublishDateTo(String publishDateTo) {
        this.publishDateTo = publishDateTo;
    }

    public String getApplicationDateFrom() {
        return applicationDateFrom;
    }

    public void setApplicationDateFrom(String applicationDateFrom) {
        this.applicationDateFrom = applicationDateFrom;
    }

    public String getApplicationDateTo() {
        return applicationDateTo;
    }

    public void setApplicationDateTo(String applicationDateTo) {
        this.applicationDateTo = applicationDateTo;
    }

    public static final class Builder {
        private SiteSearchCriteria criteria;

        public Builder() {
            criteria = new SiteSearchCriteria();
        }

        public Builder setSearchLineCriteria(List<String> lineCriteria) {
            criteria.setSearchLineCriteria(lineCriteria);
            return this;
        }

        public Builder setSortsStrings(String sortsStrings) {
            criteria.setSortsStrings(sortsStrings);
            return this;
        }

        public Builder setSearchBy44(Boolean searchBy44) {
            criteria.setSearchBy44(searchBy44);
            return this;
        }

        public Builder setSearchBy223(Boolean searchBy223) {
            criteria.setSearchBy223(searchBy223);
            return this;
        }

        public Builder setPublishDateFrom(String publishDateFrom) {
            criteria.setPublishDateFrom(publishDateFrom);
            return this;
        }

        public Builder setPublishDateTo(String publishDateTo) {
            criteria.setPublishDateTo(publishDateTo);
            return this;
        }

        public Builder setApplicationDateFrom(String applicationDateFrom) {
            criteria.setApplicationDateFrom(applicationDateFrom);
            return this;
        }

        public Builder setApplicationDateTo(String applicationDateTo) {
            criteria.setApplicationDateTo(applicationDateTo);
            return this;
        }

        public SiteSearchCriteria build() {
            return criteria;
        }
    }
}
