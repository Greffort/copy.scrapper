package com.rbbs.scrapper.parsers.util;

import com.rbbs.scrapper.shared.LoggerHelper;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ExcelCriteria {
    private static final Logger logConsole = Logger.getLogger("APP1");
    private static final Logger logFile = Logger.getLogger("APP2");

    private List<String> tableBodyCriteria;
    private List<String> doubleSelectedRowCriteria;
    private List<String> row2Criteria;
    private List<String> row3Criteria;
    private List<String> row4Criteria;
    private List<String> row5Criteria;
    private List<String> row6Criteria;


    public ExcelCriteria(String rowCriteriaFilePath, String tableBodyCriteriaFilePath, String doubleSelectedRowCriteriaFilePath) {
        File rowCriteriaFile = new File(rowCriteriaFilePath);
        File tableBodyCriteriaFile = new File(tableBodyCriteriaFilePath);
        File doubleSelectedRowCriteriaFile = new File(doubleSelectedRowCriteriaFilePath);

        //TODO Переработать логи и удалить закоменченные try или оставить try, писать в лог, но пробрасывать Exception дальше
        this.tableBodyCriteria = getTableBodyCriteria(tableBodyCriteriaFile);
        this.doubleSelectedRowCriteria = getDoubleSelectedRowCriteria(doubleSelectedRowCriteriaFile);
        this.row2Criteria = getRowCriteria(rowCriteriaFile, 1);
        this.row3Criteria = getRowCriteria(rowCriteriaFile, 2);
        this.row4Criteria = getRowCriteria(rowCriteriaFile, 3);
        this.row5Criteria = getRowCriteria(rowCriteriaFile, 4);
        this.row6Criteria = getRowCriteria(rowCriteriaFile, 5);
    }

    public List<String> getTableBodyCriteria() {
        return tableBodyCriteria;
    }

    public List<String> getDoubleSelectedRowCriteria() {
        return doubleSelectedRowCriteria;
    }

    public List<String> getRow2Criteria() {
        return row2Criteria;
    }

    public List<String> getRow3Criteria() {
        return row3Criteria;
    }

    public List<String> getRow4Criteria() {
        return row4Criteria;
    }

    public List<String> getRow5Criteria() {
        return row5Criteria;
    }

    public List<String> getRow6Criteria() {
        return row6Criteria;
    }

    @NotNull
    private List<String> getDoubleSelectedRowCriteria(File file) {
        LoggerHelper.info("getDoubleSelectedRowCriteria", logFile);

        XSSFWorkbook myExcelBook = getWorkbook(file);
        XSSFSheet myExcelSheet = myExcelBook.getSheetAt(0);

        List<String> criteriaList = new ArrayList<>();

        for (int rowCount = 0; rowCount < myExcelSheet.getPhysicalNumberOfRows(); rowCount++) {
            XSSFRow row = myExcelSheet.getRow(rowCount);
            if (row != null) {
                XSSFCell firstCell = row.getCell(0);
                if (firstCell != null) {
                    criteriaList.add(firstCell.toString());
                }
            }
        }
        return criteriaList;
    }

    @NotNull
    private List<String> getRowCriteria(File file, int index) {
        LoggerHelper.info("getRowCriteria", logFile);
        XSSFWorkbook myExcelBook = getWorkbook(file);
        XSSFSheet myExcelSheet = myExcelBook.getSheetAt(0);
        List<String> firstCriteriaList = new ArrayList<>();

        for (int rowCount = 0; rowCount < myExcelSheet.getPhysicalNumberOfRows(); rowCount++) {
            XSSFRow row = myExcelSheet.getRow(rowCount);
            if (row != null) {
                XSSFCell firstCell = row.getCell(index);
                if (firstCell == null) {
                    break;
                } else {
                    firstCriteriaList.add(firstCell.toString());
                }
            }
        }
        return firstCriteriaList;
    }

    @NotNull
    private List<String> getTableBodyCriteria(File file) {
        LoggerHelper.info("getTableBodyCriteria", logFile);
        XSSFWorkbook myExcelBook = getWorkbook(file);
        XSSFSheet myExcelSheet = myExcelBook.getSheetAt(0);

        List<String> criterions = new ArrayList<>();

        for (int rowCount = 0; rowCount < myExcelSheet.getPhysicalNumberOfRows(); rowCount++) {
            XSSFRow row = myExcelSheet.getRow(rowCount);
            if (row != null) {
                for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {
                    XSSFCell cell = row.getCell(i);
                    if (cell != null) {
                        if (!cell.toString().equals("")) {
                            criterions.add(cell.toString());
                        }
                    }
                }
            }
        }
        return criterions;
    }

    @NotNull
    private XSSFWorkbook getWorkbook(File file) {
        LoggerHelper.info("getWorkbook", logFile);
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream(file);
            return new XSSFWorkbook(fileInputStream);
        } catch (IOException e) {
            return new XSSFWorkbook();
        }
    }
}
