package com.rbbs.scrapper.parsers.site.WebDriver;

import com.rbbs.scrapper.shared.LoggerHelper;
import com.rbbs.scrapper.shared.ResourcesUtil;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import static com.rbbs.scrapper.shared.LogMessages.ChromeDriver.*;

public class RBBSChromeDriver {
    private static final Logger consoleLogger = Logger.getLogger("APP1");
    private static final Logger fileLogger = Logger.getLogger("APP2");

    @Nullable
    @Contract(" -> new")
    public static ChromeDriver getChromeDriver() {
        LoggerHelper.info(INFO_CREATING_WEB_DRIVER, fileLogger, consoleLogger);
        ChromeOptions options = new ChromeOptions()
                .addArguments("headless")
                .addArguments("--log-level=3")
                .addArguments();
        options.setCapability("--no-startup-window", true);
        System.setProperty("webdriver.chrome.driver", ResourcesUtil.WebDriverPaths.EXTERNAL_FILE_PATH);

        try {
            ChromeDriver chromeDriver = new ChromeDriver(options);
            LoggerHelper.info(INFO_SUCCESS_CREATING_WEB_DRIVER, fileLogger, consoleLogger);
            return chromeDriver;
        } catch (Exception e) {
            LoggerHelper.error(INFO_FAILED_CREATING_WEB_DRIVER, e, fileLogger, consoleLogger);
        }
        return null;
    }
}
