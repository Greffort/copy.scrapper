package com.rbbs.scrapper.parsers.site.model;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Tender {

    private static final String DEFAULT_LINK_TO_TENDER = "";
    private static final String DEFAULT_TENDER_NUMBER = "";
    private static final String DEFAULT_DESCRIPTION = "";
    private static final String DEFAULT_HOSTING_ORGANIZATION = "";
    private static final String DEFAULT_TENDER_STEP = "";
    private static final String DEFAULT_START_PRICE = "";
    private static final Map<String, String> DEFAULT_LINKS_TO_ATTACHMENTS = new HashMap<>();
    private static final String DEFAULT_DATEUPLOAD_ATTACHMENTS = "";
    private static final String DEFAULT_WINNER = "";
    private static final String DEFAULT_WINNER_INN = "";
    private static final String DEFAULT_LAST_PRICE = "";
    private static final String DEFAULT_DIRECTORY_NAME_WITH_ATTACHMENTS = "";
    private static final File DEFAULT_DIRECTORY_WITH_ATTACHMENTS = new File("C:\\FILE_NOT_FOUND");

    //link
    private String linkToTender;

    //GeneralInformationGroup
    private String tenderNumber;
    private String description;
    private String hostingOrganization;
    private String tenderStep;
    private String startPrice;

    //AttachmentsLinkGroup
    private Map<String, String> linksToAttachments;
    private String dateUploadAttachments;

    //ResultWinnerTenderLinkGroup
    private String winner;
    private String winnerINN;
    private String lastPrice;

    //EventLogLinkGroup
    // null

    // DirectoryTenderGroup
    private String directoryName;
    private File directory;

    public Tender() {
        this(DEFAULT_LINK_TO_TENDER, DEFAULT_TENDER_NUMBER,
                DEFAULT_DESCRIPTION, DEFAULT_HOSTING_ORGANIZATION,
                DEFAULT_TENDER_STEP, DEFAULT_START_PRICE,
                DEFAULT_LINKS_TO_ATTACHMENTS, DEFAULT_DATEUPLOAD_ATTACHMENTS,
                DEFAULT_WINNER, DEFAULT_WINNER_INN,
                DEFAULT_LAST_PRICE, DEFAULT_DIRECTORY_NAME_WITH_ATTACHMENTS,
                DEFAULT_DIRECTORY_WITH_ATTACHMENTS);
    }

    public Tender(String linkToTender, String tenderNumber,
                  String description, String hostingOrganization,
                  String tenderStep, String startPrice,
                  Map<String, String> linksToAttachments, String dateUploadAttachments,
                  String winner, String winnerINN,
                  String lastPrice, String directoryName,
                  File directory) {
        this.linkToTender = linkToTender;
        this.tenderNumber = tenderNumber;
        this.description = description;
        this.hostingOrganization = hostingOrganization;
        this.tenderStep = tenderStep;
        this.startPrice = startPrice;
        this.linksToAttachments = linksToAttachments;
        this.dateUploadAttachments = dateUploadAttachments;
        this.winner = winner;
        this.winnerINN = winnerINN;
        this.lastPrice = lastPrice;
        this.directoryName = directoryName;
        this.directory = directory;
    }

    public String getLinkToTender() {
        return linkToTender;
    }

    public void setLinkToTender(String linkToTender) {
        this.linkToTender = linkToTender;
    }

    public String getTenderNumber() {
        return tenderNumber;
    }

    public void setTenderNumber(String tenderNumber) {
        this.tenderNumber = tenderNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHostingOrganization() {
        return hostingOrganization;
    }

    public void setHostingOrganization(String hostingOrganization) {
        this.hostingOrganization = hostingOrganization;
    }

    public String getTenderStep() {
        return tenderStep;
    }

    public void setTenderStep(String tenderStep) {
        this.tenderStep = tenderStep;
    }

    public String getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(String startPrice) {
        this.startPrice = startPrice;
    }

    public Map<String, String> getLinksToAttachments() {
        return linksToAttachments;
    }

    public void setLinksToAttachments(Map<String, String> linksToAttachments) {
        this.linksToAttachments = linksToAttachments;
    }

    public String getDateUploadAttachments() {
        return dateUploadAttachments;
    }

    public void setDateUploadAttachments(String dateUploadAttachments) {
        this.dateUploadAttachments = dateUploadAttachments;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getWinnerINN() {
        return winnerINN;
    }

    public void setWinnerINN(String winnerINN) {
        this.winnerINN = winnerINN;
    }

    public String getLastPrice() {
        return lastPrice;
    }

    public void setLastPrice(String lastPrice) {
        this.lastPrice = lastPrice;
    }

    public String getDirectoryName() {
        return directoryName;
    }

    public void setDirectoryName(String directoryName) {
        this.directoryName = directoryName;
    }

    public File getDirectory() {
        return directory;
    }

    public void setDirectory(File directory) {
        this.directory = directory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tender tender = (Tender) o;
        return Objects.equals(linkToTender, tender.linkToTender) &&
                Objects.equals(tenderNumber, tender.tenderNumber) &&
                Objects.equals(description, tender.description) &&
                Objects.equals(hostingOrganization, tender.hostingOrganization) &&
                Objects.equals(tenderStep, tender.tenderStep) &&
                Objects.equals(startPrice, tender.startPrice) &&
                Objects.equals(linksToAttachments, tender.linksToAttachments) &&
                Objects.equals(dateUploadAttachments, tender.dateUploadAttachments) &&
                Objects.equals(winner, tender.winner) &&
                Objects.equals(winnerINN, tender.winnerINN) &&
                Objects.equals(lastPrice, tender.lastPrice) &&
                Objects.equals(directoryName, tender.directoryName) &&
                Objects.equals(directory, tender.directory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(linkToTender, tenderNumber, description, hostingOrganization, tenderStep, startPrice, linksToAttachments, dateUploadAttachments, winner, winnerINN, lastPrice, directoryName, directory);
    }

    @Override
    public String toString() {
        return "Tender{" +
                "linkToTender='" + linkToTender + '\'' +
                ", \ntenderNumber='" + tenderNumber + '\'' +
                ", \ndescription='" + description + '\'' +
                ", \nhostingOrganization='" + hostingOrganization + '\'' +
                ", \ntenderStep='" + tenderStep + '\'' +
                ", \nstartPrice='" + startPrice + '\'' +
                ", \nlinksToAttachments=" + linksToAttachments +
                ", \ndateUploadAttachments='" + dateUploadAttachments + '\'' +
                ", \nwinner='" + winner + '\'' +
                ", \nwinnerINN='" + winnerINN + '\'' +
                ", \nlastPrice='" + lastPrice + '\'' +
                ", \ndirectoryName='" + directoryName + '\'' +
                ", \ndirectory=" + directory +
                '}';
    }
}
