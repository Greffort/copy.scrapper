package com.rbbs.scrapper.parsers.site.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Site {
    private List<String> allListedTendersLinks = new ArrayList<>();
    private Set<String> allLinksToTenders = new TreeSet<>();
    private List<Tender> tenders = new ArrayList<>();

    public List<String> getAllListedTendersLinks() {
        return allListedTendersLinks;
    }

    public void setAllListedTendersLinks(List<String> allListedTendersLinks) {
        this.allListedTendersLinks = allListedTendersLinks;
    }

    public Set<String> getAllLinksToTenders() {
        return allLinksToTenders;
    }

    public void setAllLinksToTenders(Set<String> allLinksToTenders) {
        this.allLinksToTenders = allLinksToTenders;
    }

    public List<Tender> getTenders() {
        return tenders;
    }

    public void setTenders(List<Tender> tenders) {
        this.tenders = tenders;
    }
}
