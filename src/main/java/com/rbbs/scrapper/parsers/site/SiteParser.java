package com.rbbs.scrapper.parsers.site;

import com.rbbs.scrapper.parsers.site.WebDriver.RBBSChromeDriver;
import com.rbbs.scrapper.parsers.site.model.Site;
import com.rbbs.scrapper.parsers.site.model.Tender;
import com.rbbs.scrapper.parsers.util.SiteSearchCriteria;
import com.rbbs.scrapper.shared.LoggerHelper;
import com.rbbs.scrapper.shared.ResourcesUtil;
import com.rbbs.scrapper.util.file_storages.WindowsFileStorage;
import com.rbbs.scrapper.util.file_storages.interfaces.FileStorage;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.rbbs.scrapper.shared.LogMessages.SiteParser.*;
import static com.rbbs.scrapper.util.MomKiller.isProcessRunning;
import static com.rbbs.scrapper.util.MomKiller.killProcess;

public class SiteParser implements AutoCloseable {
    private final AtomicBoolean isRunning = new AtomicBoolean(false);
    private static final Logger consoleLogger = Logger.getLogger("APP1");
    private static final Logger fileLogger = Logger.getLogger("APP2");
    private static final String ENABLE_SEARCH_BY_LOW = "on";
    private List<String> allExtensions = new ArrayList<>(Arrays.asList(".xls", ".xlsx", ".pdf", ".rar", ".zip", ".7z", ".XLS", ".XLSX", ".PDF", ".RAR", ".ZIP", ".7Z"));

    private SiteSearchCriteria siteSearchCriteria;
    private ChromeDriver browserDriver;
    private String pathToDirectory;

    public SiteParser(SiteSearchCriteria siteSearchCriteria, String pathToDirectory) {
        this.siteSearchCriteria = siteSearchCriteria;
        this.pathToDirectory = pathToDirectory;
        isRunning.set(true);
    }

    public Site getSite() {
        Site site = new Site();
        if (isRunning.get()) {
            this.browserDriver = RBBSChromeDriver.getChromeDriver();
            List<String> linkToListedTenders = getListedTendersLinks();

            if (linkToListedTenders.size() > 0) {
                site.setAllListedTendersLinks(linkToListedTenders);

                Set<String> linksToTenders = getAllTendersLinks(linkToListedTenders);
                if (linksToTenders.size() > 0) {
                    site.setAllLinksToTenders(linksToTenders);

                    List<Tender> tenders = getTenders(linksToTenders);
                    site.setTenders(tenders);
                }
            }
        }
        return site;
    }

    @NotNull
    private List<String> getListedTendersLinks() {
        LoggerHelper.info("getListedTendersLinks",fileLogger);
        List<String> resultDocList = new ArrayList<>();
        List<String> searchStrings = siteSearchCriteria.getSearchLineCriteria();

        for (String criterion : searchStrings) {
            if (isRunning.get()) {
                String urlRequest = getURLRequest(criterion, 1);
                Document doc = getHTMLPageJsoup(urlRequest);
                if (doc != null) {
                    int countPages = getCountPages(doc.getElementsByClass("pages"));

                    if (countPages == 0) {
                        resultDocList.add(urlRequest);
                    }
                    for (int j = 0; j < countPages; j++) {
                        resultDocList.add(getURLRequest(criterion, j + 1));
                    }
                }
            } else return Collections.emptyList();
        }
        return resultDocList;
    }

    @NotNull
    private Set<String> getAllTendersLinks(@NotNull List<String> linkToListedTenders) {
        LoggerHelper.info("getAllTendersLinks",fileLogger);
        Set<String> links = new TreeSet<>();

        for (String link : linkToListedTenders) {
            if (isRunning.get()) {
                Document doc = getHTMLPageJsoup(link);
                if (doc != null) {
                    Elements elements = doc.getElementsByClass("registry-entry__header-mid__number");
                    links.addAll(getTenderLinks(elements));
                }
            } else return Collections.emptySet();
        }
        return links;
    }

    @NotNull
    private List<String> getTenderLinks(@NotNull Elements elements) {
        LoggerHelper.info("getTenderLinks",fileLogger);
        List<String> links = new ArrayList<>();
        boolean searchBy44 = siteSearchCriteria.isSearchBy44();
        boolean searchBy223 = siteSearchCriteria.isSearchBy223();

        String[] list = elements.toString().split("</div>");
        String partURL = "https://zakupki.gov.ru";
        String firstCheck = "href=\"";
        String lastCheck = "\"> №";

        for (String s : list) {
            String line = s.trim();

            int first1 = line.indexOf(firstCheck);
            int last1 = line.indexOf(lastCheck);

            if (first1 != -1 || last1 != -1) {
                String subString = line.substring(first1 + firstCheck.length(), last1).trim();
                if (searchBy44) {
                    links.add(partURL + subString);
                } else if (searchBy223) {
                    links.add(subString);
                }
            }
        }
        return links;
    }

    @NotNull
    private List<Tender> getTenders(@NotNull Set<String> linksToTenders) {
        LoggerHelper.info("getTenderS",fileLogger);
        List<Tender> tenders = new ArrayList<>();

        for (String link : linksToTenders) {
            if (isRunning.get()) {
                tenders.add(getTender(link));
            } else return Collections.emptyList();
        }
        return tenders;
    }

    @NotNull
    private Tender getTender(String linkToTender) {
        LoggerHelper.info("getTender",fileLogger);
        LoggerHelper.info(INFO_PARSING_TENDER_LINK + linkToTender, fileLogger, consoleLogger);

        Tender tender = new Tender();
        Map<String, String> pagesLinks;
        setLinkToTender(tender, linkToTender);

        if (siteSearchCriteria.isSearchBy44()) {
            pagesLinks = getLinksToPagesTender(linkToTender);
            Document docGeneralInformationGroup = getHTMLPageJsoup(pagesLinks.get("generalInformationLink"));
            Document docAttachmentsLinkGroup = getHTMLPageSelenium(pagesLinks.get("attachmentsLink"));
            Document docResultWinnerTenderLinkGroup = getHTMLPageSelenium(pagesLinks.get("resultWinnerTenderLink"));
            Document docEventLogLinkGroup = getHTMLPageSelenium(pagesLinks.get("eventLogLink"));

            if (docGeneralInformationGroup != null) {
                setGeneralInformationGroupForTender(tender, docGeneralInformationGroup);

                setDirectoryTenderGroupForTender(tender);
                setDirectory(tender, pathToDirectory);
            }
            if (docAttachmentsLinkGroup != null) {
                setAttachmentsLinkGroupForTender(tender, docAttachmentsLinkGroup);
                downloadAttachments(tender);
            }
            if (docResultWinnerTenderLinkGroup != null) {
                setResultWinnerTenderLinkGroupForTender(tender, docResultWinnerTenderLinkGroup);
            }
            if (docEventLogLinkGroup != null) {
                setEventLogLinkGroupForTender(tender, docEventLogLinkGroup);
            }
        } else if (siteSearchCriteria.isSearchBy223()) {
            pagesLinks = getLinksToPagesTenderFZ223(linkToTender);

            Document docGeneralInformationGroup = getHTMLPageJsoup(pagesLinks.get("generalInformationLink"));
            Document docGeneralLotGroup = getHTMLPageJsoup(pagesLinks.get("lot-list"));
            Document docAttachmentsLinkGroup = getHTMLPageJsoup(pagesLinks.get("attachmentsLink"));

            if (docGeneralInformationGroup != null) {
                tender.setTenderNumber(getElementGeneralInformatioGroupBy223(docGeneralInformationGroup, "noticeTabBoxWrapper", "</tr>", "Реестровый номер извещения", "<span>", "</span>"));
                tender.setDescription(getElementGeneralInformatioGroupBy223(docGeneralInformationGroup, "noticeTabBoxWrapper", "</tr>", "Наименование закупки", "<span>", "</span>"));
                tender.setHostingOrganization(getElementGeneralInformatioGroupBy223(docGeneralInformationGroup, "noticeTabBoxWrapper", "</tr>", "Наименование организации", "\">", "</a>"));
                setDirectoryName(tender);
                setDirectory(tender, pathToDirectory);
            }
            if (docGeneralLotGroup != null) {
                tender.setStartPrice(getStartPriceBy223(docGeneralLotGroup));
            }
            if (docAttachmentsLinkGroup != null) {
                tender.setLinksToAttachments(getLinksToAttachmentsBy223(docAttachmentsLinkGroup));
                downloadAttachmentsBy223(tender);
            }
        }

        LoggerHelper.info(INFO_SUCCESS, fileLogger, consoleLogger);
        return tender;
    }

    @NotNull
    private Map<String, String> getLinksToPagesTender(@NotNull String linkToTender) {
        Map<String, String> resultMap = new HashMap<>();

        String attachmentsLink = linkToTender.replaceFirst("common-info", "documents");
        String resultWinnerTenderLink = linkToTender.replaceFirst("common-info", "supplier-results");
        String eventLogLink = linkToTender.replaceFirst("common-info", "event-journal");

        resultMap.put("generalInformationLink", linkToTender);
        resultMap.put("attachmentsLink", attachmentsLink);
        resultMap.put("resultWinnerTenderLink", resultWinnerTenderLink);
        resultMap.put("eventLogLink", eventLogLink);
        return resultMap;
    }

    @NotNull
    private Map<String, String> getLinksToPagesTenderFZ223(@NotNull String linkToTender) {
        Map<String, String> resultMap = new HashMap<>();

        String attachmentsLink = linkToTender.replaceFirst("common-info", "documents");
        String lotList = linkToTender.replaceFirst("common-info", "lot-list");

        resultMap.put("generalInformationLink", linkToTender);
        resultMap.put("attachmentsLink", attachmentsLink);
        resultMap.put("lot-list", lotList);

        return resultMap;
    }

    private void setLinkToTender(@NotNull Tender tender, String link) {
        tender.setLinkToTender(link);
    }

    public String getElementGeneralInformatioGroupBy223(@NotNull Document doc, String byElements, String firtsSplit, String searchString, String firstCheck, String lastCheck) {
        Elements elements = doc.getElementsByClass(byElements);

        String[] list = elements.toString().split(firtsSplit);

        String result = "";
        for (String s : list) {
            String line1 = s.trim();

            if (line1.contains(searchString)) {
                int firstIndexOf = line1.lastIndexOf(firstCheck);
                int lastIndexOf = line1.lastIndexOf(lastCheck);
                try {
                    result = line1.substring(firstIndexOf + firstCheck.length(), lastIndexOf).replaceAll("&nbsp;", " ").trim();
                } catch (StringIndexOutOfBoundsException e) {
                    LoggerHelper.error(ERROR_SUBSTRING, e, fileLogger);
                }
            }
        }
        return result;
    }

    private String getStartPriceBy223(@NotNull Document doc) {
        String searchString = "Сведения о лотах";
        String searchString2 = "Сведения о цене договора";

        Elements elements = doc.getElementsByClass("noticeTabBox padBtm20");
        String[] list = elements.toString().split("<h2 class=\"noticeBoxH2\">");

        String result = "";
        for (String s : list) {
            String line1 = s.trim();
            if (line1.contains(searchString)) {
                Document doc1 = Jsoup.parse(line1);
                Elements elements1 = doc1.getElementsByTag("th");
                Elements elements2 = doc1.getElementsByTag("td");

                String[] tableHeads = elements1.toString().split("</th>");
                String[] tableBodies = elements2.toString().split("</td>");

                for (int j = 0; j < tableHeads.length; j++) {
                    String tableHesdsLine = tableHeads[j];
                    if (tableHesdsLine.contains(searchString2)) {
                        result = tableBodies[j].replaceAll("&nbsp;", " ").replaceAll("<td>", " ").trim();
                    }
                }
            }
        }
        return result;
    }

    private void downloadAttachmentsBy223(@NotNull Tender tender) {
        try {
            for (Map.Entry<String, String> entry : tender.getLinksToAttachments().entrySet()) {
                URL url = new URL(entry.getValue());
                URLConnection con = url.openConnection();

                String fieldValue = con.getHeaderField("Content-Disposition");

                if (fieldValue != null || fieldValue.contains("filename=\"")) {

                    int first = fieldValue.indexOf("filename=\"");
                    int last = fieldValue.indexOf("\";");
                    String filename;
                    String result;
//                    if (last == -1) {
//                        filename = fieldValue.substring(first + "filename=\"".length());
//                        result = java.net.URLDecoder.decode(filename, StandardCharsets.UTF_8.name());
//                    }
                    if (first != -1 && last != -1) {
                        filename = fieldValue.substring(first + "filename=\"".length(), last);
                        result = java.net.URLDecoder.decode(filename, StandardCharsets.UTF_8.name());
                        boolean flag = false;
                        for (String ext : allExtensions) {
                            if (result.endsWith(ext)) {
                                LoggerHelper.info(INFO_DOWNLOADING_FILE + result + "... ", fileLogger, consoleLogger);
                                File file = new File(pathToDirectory + "\\" + tender.getDirectoryName() + "\\Документы_закупки\\" + result);
                                FileUtils.copyURLToFile(new URL(entry.getValue()), file, 10000, 10000);
                                flag = true;
                            }
                        }
                        if (!flag) {
                            LoggerHelper.info(INFO_SKIPPING_FILE + result + "... ", fileLogger, consoleLogger);
                        }
                    }
                }
            }
        } catch (IOException e) {
            LoggerHelper.error(ERROR_DOWNLOADING_FILE, e, fileLogger);
        }
    }

    public void downloadAttachments(@NotNull Tender tender) {
        try {
            for (Map.Entry<String, String> entry : tender.getLinksToAttachments().entrySet()) {
                boolean flag = false;
                for (String ext : allExtensions) {
                    if (entry.getKey().endsWith(ext)) {
                        LoggerHelper.info(INFO_DOWNLOADING_FILE + entry.getKey() + "... ", fileLogger, consoleLogger);
                        File file = new File(pathToDirectory + "\\" + tender.getDirectoryName() + "\\Документы_закупки\\" + entry.getKey());
                        FileUtils.copyURLToFile(new URL(entry.getValue()), file, 10000, 10000);
                        flag = true;
                    }
                }
                if(!flag){
                    fileLogger.info("Skipping file - " + entry.getKey() + "... ");
                    consoleLogger.info("Skipping file - " + entry.getKey() + "... ");
                }
            }
        } catch (IOException e) {
            fileLogger.error(Arrays.toString(e.getStackTrace()));
            fileLogger.info("Downloading file - FAILED\n");
            consoleLogger.info("Downloading file - FAILED\n");
        }
    }

    @NotNull
    private Map<String, String> getLinksToAttachmentsBy223(@NotNull Document doc) {
        Elements elements = doc.getElementsByClass("noticeTabBox");
        String[] list = elements.toString().split("<h2>");
        String attachmentsBlock = "Документация по закупке";
        String partLink = "https://zakupki.gov.ru";
        boolean flag = false;
        Map<String, String> results = new HashMap<>();

        for (String value : list) {
            if (flag) break;
            String line1 = value.trim();
            if (line1.contains(attachmentsBlock)) {

                Document doc2 = Jsoup.parse(line1);
                Elements elementsRow = doc2.getElementsByClass("epz_aware");

                String[] list1 = elementsRow.toString().split("\n");

                for (String s : list1) {
                    String tableHesdsLine = s.replaceAll("&nbsp;", " ").trim();

                    if (!tableHesdsLine.equals("")) {
                        try {
                            String firstCheck = "href=\"";
                            String lastCheck = "\" title";
                            int firstIndexOf = tableHesdsLine.lastIndexOf(firstCheck);
                            int lastIndexOf = tableHesdsLine.lastIndexOf(lastCheck);
                            String linkToFile = tableHesdsLine.substring(firstIndexOf + firstCheck.length(), lastIndexOf).replaceAll("&nbsp;", " ").trim();

                            String firstCheck2 = "\">";
                            String lastCheck2 = "</a>";
                            int firstIndexOf2 = tableHesdsLine.lastIndexOf(firstCheck2);
                            int lastIndexOf2 = tableHesdsLine.lastIndexOf(lastCheck2);
                            String fileName = tableHesdsLine.substring(firstIndexOf2 + firstCheck2.length(), lastIndexOf2).replaceAll("&nbsp;", " ").trim();

                            results.put(fileName, partLink + linkToFile);
                            flag = true;
                        } catch (StringIndexOutOfBoundsException e) {
                            LoggerHelper.error(ERROR_SUBSTRING, e, fileLogger);
                        }
                    }
                }
            }
        }
        return results;
    }

    private void setGeneralInformationGroupForTender(Tender tender, Document doc) {
        setTenderNumber(tender, doc);
        setDescription(tender, doc);
        setHostingOrganization(tender, doc);
        setTenderStep(tender, doc);
        setStartPrice(tender, doc);
    }

    private void setTenderNumber(Tender tender, @NotNull Document doc) {
        Elements elements = doc.getElementsByClass("cardMainInfo__purchaseLink distancedText");
        String line = elements.toString();
        String firstCheck = "_blank\">";
        String lastCheck = "</a>";
        int firstIndexOf = line.indexOf(firstCheck);
        int lastIndexOf = line.indexOf(lastCheck);

        String result = "";
        try {
            result = line.substring(firstIndexOf + firstCheck.length(), lastIndexOf).replaceAll("№", "").trim();
        } catch (StringIndexOutOfBoundsException e) {
            LoggerHelper.error(ERROR_SUBSTRING, e, fileLogger);
        }
        tender.setTenderNumber(result);
    }

    private void setDescription(Tender tender, @NotNull Document doc) {
        Elements elements = doc.getElementsByClass("sectionMainInfo__body");
        String[] list = elements.toString().split("</div>");
        String description = "Объект закупки";

        String result = "";
        for (String s : list) {
            String line = s.trim();
            if (line.contains(description)) {
                String firstCheck = "<span class=\"cardMainInfo__content\">";
                String lastCheck = "</span>";
                int firstIndexOf = line.indexOf(firstCheck);
                int lastIndexOf = line.lastIndexOf(lastCheck);
                try {
                    result = line.substring(firstIndexOf + firstCheck.length(), lastIndexOf).trim();
                } catch (StringIndexOutOfBoundsException e) {
                    LoggerHelper.error(ERROR_SUBSTRING, e, fileLogger);
                }
            }
        }
        tender.setDescription(result);
    }

    private void setHostingOrganization(Tender tender, @NotNull Document doc) {
        Elements elements = doc.getElementsByClass("blockInfo__section section");
        String[] list = elements.toString().split("</div>");
        String hostingOrganization = "Размещение осуществляет";

        String result = "";
        for (String s : list) {
            String line1 = s.trim();
            String[] list2 = line1.split("</section>");

            for (String value : list2) {
                String line = value.trim();
                if (line.contains(hostingOrganization)) {
                    String firstCheck = "\">";
                    String lastCheck = "</a> </span>";
                    int firstIndexOf = line.lastIndexOf(firstCheck);
                    int lastIndexOf = line.lastIndexOf(lastCheck);
                    try {
                        result = line.substring(firstIndexOf + firstCheck.length(), lastIndexOf).trim();
                    } catch (StringIndexOutOfBoundsException e) {
                        LoggerHelper.error(ERROR_SUBSTRING, e, fileLogger);
                    }
                }
            }
        }
        tender.setHostingOrganization(result);
    }

    private void setTenderStep(Tender tender, @NotNull Document doc) {
        Elements elements = doc.getElementsByClass("blockInfo__section section");
        String[] list = elements.toString().split("</div>");
        String tenderStep = "Этап закупки";
        String error = "Приостановлено";
        String result = "";
        for (String s : list) {
            String line1 = s.trim();
            String[] list2 = line1.split("</section>");

            for (String value : list2) {
                String line = value.trim();
                if (line.contains(tenderStep)) {
                    String firstCheck = "\">";
                    String lastCheck = "</span>";
                    int firstIndexOf = line.lastIndexOf(firstCheck);
                    int lastIndexOf = line.lastIndexOf(lastCheck);
                    try {
                        result = line.substring(firstIndexOf + firstCheck.length(), lastIndexOf).trim();
                    } catch (StringIndexOutOfBoundsException e) {
                        LoggerHelper.error(ERROR_SUBSTRING, e, fileLogger);
                    }
                    if (line.contains(error)) {
                        result += " (" + error + ")";
                    }
                }
            }
        }
        tender.setTenderStep(result);
    }

    private void setStartPrice(Tender tender, @NotNull Document doc) {
        Elements elements = doc.getElementsByClass("blockInfo__section section");
        String[] list = elements.toString().split("</div>");
        String startPrice = "Начальная (максимальная) цена контракта";

        String result = "";
        for (String s : list) {
            String line1 = s.trim();
            String[] list2 = line1.toString().split("</section>");
            for (String value : list2) {
                String line = value.trim();

                if (line.contains(startPrice)) {
                    String firstCheck = "\">";
                    String lastCheck = "</span>";
                    int firstIndexOf = line.lastIndexOf(firstCheck);
                    int lastIndexOf = line.lastIndexOf(lastCheck);
                    try {
                        result = line.substring(firstIndexOf + firstCheck.length(), lastIndexOf).replaceAll("&nbsp;", " ").trim();
                    } catch (StringIndexOutOfBoundsException e) {
                        LoggerHelper.error(ERROR_SUBSTRING, e, fileLogger);
                    }
                }
            }
        }
        tender.setStartPrice(result);
    }

    private void setAttachmentsLinkGroupForTender(Tender tender, Document doc) {
        setLinksToAttachments(tender, doc);
        setDateUploadAttachments(tender, doc);
    }

    private void setLinksToAttachments(Tender tender, @NotNull Document doc) {
        Elements elements = doc.getElementsByClass("col-sm-12 blockInfo");
        String[] list = elements.toString().split("<div class=\"col-sm-12 blockInfo\">");
        String attachmentsBlock = "Документация, изменение документации";

        boolean flag = false;
        Map<String, String> results = new HashMap<>();

        for (String value : list) {
            if (flag) break;
            String line1 = value.trim();

            if (line1.contains(attachmentsBlock)) {
                Document doc2 = Jsoup.parse(line1);
                Elements elementsRow = doc2.getElementsByClass("attachment row ");
                elementsRow.addAll(doc2.getElementsByClass("attachment row displayNone closedFilesDocs"));
                String[] list1 = elementsRow.toString().split("</div>");

                for (String s : list1) {
                    String tableHesdsLine = s.replaceAll("&nbsp;", " ").trim();

                    if (!tableHesdsLine.equals("")) {
                        try {
                            String firstCheck = "<a href=\"";
                            String lastCheck = "\" title";
                            int firstIndexOf = tableHesdsLine.lastIndexOf(firstCheck);
                            int lastIndexOf = tableHesdsLine.lastIndexOf(lastCheck);
                            String linkToFile = tableHesdsLine.substring(firstIndexOf + firstCheck.length(), lastIndexOf).replaceAll("&nbsp;", " ").trim();

                            String firstCheck2 = "title=\"";
                            String lastCheck2 = "\">";
                            int firstIndexOf2 = tableHesdsLine.lastIndexOf(firstCheck2);
                            int lastIndexOf2 = tableHesdsLine.lastIndexOf(lastCheck2);
                            String fileName = tableHesdsLine.substring(firstIndexOf2 + firstCheck2.length(), lastIndexOf2).replaceAll("&nbsp;", " ").trim();

                            results.put(fileName, linkToFile);
                            flag = true;
                        } catch (StringIndexOutOfBoundsException e) {
                            LoggerHelper.error(ERROR_SUBSTRING, e, fileLogger);
                        }
                    }
                }
            }
        }
        tender.setLinksToAttachments(results);
    }

    private void setDateUploadAttachments(Tender tender, Document doc) {
//        tender.setDateUploadAttachments(LocalDateTime.now().toString());
    }

//    public void downloadAttachments(@NotNull Tender tender) {
//        try {
//            for (Map.Entry<String, String> entry : tender.getLinksToAttachments().entrySet()) {
//                boolean flag = false;
//                for (String ext : allExtensions) {
//                    if (entry.getKey().endsWith(ext)) {
//                        fileLogger.info("Downloading file - " + entry.getKey() + "... ");
//                        consoleLogger.info("Downloading file - " + entry.getKey() + "... ");
//                        File file = new File(pathToDirectory + "\\" + tender.getDirectoryName() + "\\Документы_закупки\\" + entry.getKey());
//                        FileUtils.copyURLToFile(new URL(entry.getValue()), file, 10000, 10000);
//                        flag = true;
//                    }
//                }
//                if (!flag) {
//                    fileLogger.info("Skipping file - " + entry.getKey() + "... ");
//                    consoleLogger.info("Skipping file - " + entry.getKey() + "... ");
//                }
//            }
//        } catch (IOException e) {
//            fileLogger.error(Arrays.toString(e.getStackTrace()));
//            fileLogger.info("Downloading file - FAILED\n");
//            consoleLogger.info("Downloading file - FAILED\n");
//        }
//    }

    private void setResultWinnerTenderLinkGroupForTender(Tender tender, Document doc) {
        setWinner(tender, doc);
        setWinnerINN(tender, doc);
        setLastPrice(tender, doc);
    }

    private void setWinner(Tender tender, @NotNull Document doc) {
        String winner = "Информация о процедуре заключения контракта";
        String checkWinner = "Поставщик";

        Elements elements = doc.getElementsByClass("row blockInfo");
        String[] list = elements.toString().split("<div class=\"row blockInfo\"> ");

        String result = "";
        for (String s : list) {
            String line1 = s.trim();

            if (line1.contains(winner)) {
                Document doc1 = Jsoup.parse(line1);
                Elements elements1 = doc1.getElementsByClass("tableBlock__col tableBlock__col_header");
                Elements elements2 = doc1.getElementsByClass("tableBlock__body");

                String[] tableHeads = elements1.toString().split("<th");
                String[] tableBodies = elements2.toString().split("<td");

                for (int j = 0; j < tableHeads.length; j++) {
                    String tableHesdsLine = tableHeads[j];
                    if (tableHesdsLine.contains(checkWinner)) {
                        String firstCheck = "class=\"tableBlock__col\">";
                        String lastCheck = "</td>";
                        int firstIndexOf = tableBodies[j].lastIndexOf(firstCheck);
                        int lastIndexOf = tableBodies[j].lastIndexOf(lastCheck);
                        try {
                            result = tableBodies[j].substring(firstIndexOf + firstCheck.length(), lastIndexOf).replaceAll("&nbsp;", " ").trim();
                        } catch (StringIndexOutOfBoundsException e) {
                            LoggerHelper.error(ERROR_SUBSTRING, e, fileLogger);
                        }
                    }
                }
            }
        }
        tender.setWinner(result);
    }

    private void setWinnerINN(Tender tender, Document doc) {
        String linkToSPEC = getLinkToRPEC(doc);
        if (!linkToSPEC.isEmpty()) {
            Document doc1 = getHTMLPageSelenium(linkToSPEC);
            if (doc1 != null) {
                Elements elements = doc1.getElementsByClass("row blockInfo");

                String[] list = elements.toString().split("<div class=\"row blockInfo\"> ");

                String winner = "Информация о поставщике";
                String winnerINN = "ИНН";
                String result = "";

                for (String s : list) {
                    String line1 = s.trim();
                    if (line1.contains(winner)) {
                        String[] list1 = elements.toString().split("<div class=\"row blockInfo\"> ");
                        for (String s1 : list1) {
                            if (s1.contains(winnerINN)) {
                                String firstCheck = "<span class=\"section__info\">";
                                String lastCheck = "</span>";
                                int firstIndexOf = s1.lastIndexOf(firstCheck);
                                int lastIndexOf = s1.lastIndexOf(lastCheck);
                                try {
                                    result = s1.substring(firstIndexOf + firstCheck.length(), lastIndexOf).replaceAll("&nbsp;", " ").trim();
                                } catch (StringIndexOutOfBoundsException e) {
                                    LoggerHelper.error(ERROR_SUBSTRING, e, fileLogger);
                                }
                            }
                        }
                    }
                }
                tender.setWinnerINN(result);
            }
        }
    }

    private String getLinkToRPEC(@NotNull Document doc) {
        Elements elements = doc.getElementsByClass("row blockInfo");
        String[] list = elements.toString().split("<div class=\"row blockInfo\"> ");
        String partLink = "https://zakupki.gov.ru";
        String winner = "Информация о процедуре заключения контракта";
        String checkWinner = "Статус";
        String result = "";

        boolean flag = false;
        for (String s : list) {
            if (flag) break;
            String line1 = s.trim();

            if (line1.contains(winner)) {
                Document doc3 = Jsoup.parse(line1);
                Elements elements1 = doc3.getElementsByClass("tableBlock__col tableBlock__col_header");
                Elements elements2 = doc3.getElementsByClass("tableBlock__body");

                String[] tableHeads = elements1.toString().split("<th");
                String[] tableBodies = elements2.toString().split("<td");

                for (int j = 0; j < tableHeads.length; j++) {
                    if (flag) break;
                    String tableHesdsLine = tableHeads[j];
                    if (tableHesdsLine.contains(checkWinner)) {
                        String firstCheck = "<a href=\"";
                        String lastCheck = "\" target";
                        int firstIndexOf = tableBodies[j].lastIndexOf(firstCheck);
                        int lastIndexOf = tableBodies[j].lastIndexOf(lastCheck);
                        try {
                            result = partLink + tableBodies[j].substring(firstIndexOf + firstCheck.length(), lastIndexOf).replaceAll("&nbsp;", " ").trim();
                            flag = true;
                        } catch (StringIndexOutOfBoundsException e) {
                            LoggerHelper.error(ERROR_SUBSTRING, e, fileLogger);
                        }
                    }
                }
            }
        }
        return result;
    }

    private void setLastPrice(Tender tender, @NotNull Document doc) {
        Elements elements = doc.getElementsByClass("row blockInfo");

        String[] list = elements.toString().split("<div class=\"row blockInfo\"> ");

        String winner = "Информация о процедуре заключения контракта";
        String checkWinner = "Цена контракта";

        String result = "";
        for (String s : list) {
            String line1 = s.trim();

            if (line1.contains(winner)) {
                Document doc1 = Jsoup.parse(line1);
                Elements elements1 = doc1.getElementsByClass("tableBlock__col tableBlock__col_header");
                Elements elements2 = doc1.getElementsByClass("tableBlock__body");

                String[] tableHeads = elements1.toString().split("<th");
                String[] tableBodies = elements2.toString().split("<td");

                for (int j = 0; j < tableHeads.length; j++) {
                    String tableHesdsLine = tableHeads[j].replaceAll("&nbsp;", " ");

                    if (tableHesdsLine.contains(checkWinner)) {
                        String firstCheck = "class=\"tableBlock__col\">";
                        String lastCheck = "</td>";
                        int firstIndexOf = tableBodies[j].lastIndexOf(firstCheck);
                        int lastIndexOf = tableBodies[j].lastIndexOf(lastCheck);
                        try {
                            result = tableBodies[j].substring(firstIndexOf + firstCheck.length(), lastIndexOf).replaceAll("&nbsp;", " ").trim();
                        } catch (StringIndexOutOfBoundsException e) {
                            LoggerHelper.error(ERROR_SUBSTRING, e, fileLogger);
                        }
                    }
                }
            }
        }
        tender.setLastPrice(result);
    }

    private void setEventLogLinkGroupForTender(Tender tender, Document doc) {
        // пока нет параметров
    }

    private void setDirectoryTenderGroupForTender(Tender tender) {
        setDirectoryName(tender);
    }

    private void setDirectoryName(@NotNull Tender tender) {
        String result = "";
        result = LocalDate.now().toString().replaceAll(":", "-") + "_" + tender.getTenderNumber();
        //дата, номер закупки,
        tender.setDirectoryName(result);
    }

    private void setDirectory(@NotNull Tender tender, String pathToDirectory) {
        FileStorage fileStorage = new WindowsFileStorage();
        tender.setDirectory(fileStorage.createDirectory(pathToDirectory, tender.getDirectoryName()));
    }

    @NotNull
    private String getURLRequest(String searchString, int page) {
        String sortsStrings = siteSearchCriteria.getSortsStrings();
        boolean searchBy44 = siteSearchCriteria.isSearchBy44();
        boolean searchBy223 = siteSearchCriteria.isSearchBy223();
        String publishDateFrom = siteSearchCriteria.getPublishDateFrom();
        String publishDateTo = siteSearchCriteria.getPublishDateTo();
        String applicationDateFrom = siteSearchCriteria.getApplicationDateFrom();
        String applicationDateTo = siteSearchCriteria.getApplicationDateTo();

        StringBuilder resultLine = new StringBuilder();
        resultLine.append("https://zakupki.gov.ru/epz/order/extendedsearch/results.html")
                .append("?searchString=")
                .append(searchString)
                .append("&morphology=on")
                .append("&search-filter=Дате+размещения")
                .append("&pageNumber=")
                .append(page)
                .append("&sortDirection=false")
                .append("&recordsPerPage=_50")
                .append("&showLotsInfoHidden=false")
                .append("&sortBy=")
                .append(sortsStrings);

        if (searchBy44) {
            resultLine.append("&fz44=");
            resultLine.append(ENABLE_SEARCH_BY_LOW);
        }
        if (searchBy223) {
            resultLine.append("&fz223=");
            resultLine.append(ENABLE_SEARCH_BY_LOW);
        }

        resultLine.append("&pc=on")
                .append("&selectedSubjectsIdNameHidden=%7B%7D")
                .append("&publishDateFrom=")
                .append(publishDateFrom)
                .append("&publishDateTo=")
                .append(publishDateTo)
                .append("&applSubmissionCloseDateFrom=")
                .append(applicationDateFrom)
                .append("&applSubmissionCloseDateTo=")
                .append(applicationDateTo)
                .append("&currencyIdGeneral=-1")
                .append("&delKladrIds=5277374")
                .append("&delKladrIdsCodes=63000000000")
                .append("&OrderPlacementSmallBusinessSubject=on")
                .append("&OrderPlacementRnpData=on")
                .append("&OrderPlacementExecutionRequirement=on")
                .append("&orderPlacement94_0=0")
                .append("&orderPlacement94_1=0")
                .append("&orderPlacement94_2=0");

        return resultLine.toString();
    }

    @Nullable
    private Document getHTMLPageSelenium(String lineURL) {
        try {
            this.browserDriver.get(lineURL);
            return Jsoup.parse(browserDriver.getPageSource());
        } catch (Exception e) {
            LoggerHelper.error(ERROR_DOWNLOADING_WEB_PAGE + lineURL, e, fileLogger);
            return null;
        }
    }

    @Nullable
    private Document getHTMLPageJsoup(String lineURL) {
        try {
            return Jsoup.connect(lineURL).get();
        } catch (IOException e) {
            LoggerHelper.error(ERROR_DOWNLOADING_WEB_PAGE + lineURL, e, fileLogger);
            return null;
        }
    }

    private int getCountPages(@NotNull Elements pages) {
        String[] elPages = pages.toString().split("class=\"page\"");
        Pattern pattern1 = Pattern.compile("javascript:goToPage\\(\\d{1,5}\\)");
        Pattern pattern2 = Pattern.compile("\\d+");
        Matcher matcher1;
        Matcher matcher2;

        int maxNumber = 0;
        for (String elPage : elPages) {
            matcher1 = pattern1.matcher(elPage);
            if (matcher1.find()) {
                matcher2 = pattern2.matcher(matcher1.group());
                if (matcher2.find()) {
                    maxNumber = Integer.parseInt(matcher2.group());
                }
            }
        }
        return maxNumber;
    }

    @Override
    public void close() {
        if (browserDriver != null) {
            browserDriver.quit();
            if (isProcessRunning(ResourcesUtil.WebDriverPaths.FILE_NAME)) {
                killProcess(ResourcesUtil.WebDriverPaths.FILE_NAME);
            }
        }
    }

    public void terminate() {
        isRunning.set(false);
        LoggerHelper.info(INFO_SITE_PARSER_TEMINATED, consoleLogger, fileLogger);
    }
}
